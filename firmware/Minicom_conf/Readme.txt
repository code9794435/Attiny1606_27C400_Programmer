Example configuration for Minicom to echo entered commands and correct line breaks.

Firmware <= 1.0.6 (use files from directory 1.0.6)
	Prior firmware v1.0.7 LF was used for newline detection but Minicom only uses CR for a newline.
	To get Minicom send LF instead of CR use the following steps.

	Copy the file ".minicom_cr2lf" to your home directory. This file converts CR to LF when pressing enter.
	If you have no own configuration copy ".minirc.dfl" in your home directory and change the "port" "updir" and "downdir" Parameters to your liking.
	If you already have an own configuration file add/change the following lines to your config:
		pu convf            .minicom_cr2lf
		pu addcarreturn     Yes
		pu localecho        Yes

Firmware >= 1.0.7 (use files from directory 1.0.7)
	If you have no own configuration copy ".minirc.dfl" in your home directory and change the "port" "updir" and "downdir" Parameters to your liking.
	If you already have an own configuration file add/change the following lines to your config:
		pu addcarreturn     Yes
		pu localecho        Yes

Example configuration for Term to echo entered commands and correct line breaks.

Firmware <= 1.0.6 (use files from directory 1.0.6)
	Prior firmware v1.0.7 LF was used for newline detection but Term only uses CR for a newline.
	To get Term send LF instead of CR use the following steps.

	Copy the file "term.prefs" to the "Config" subdirectory within the Term installation.

Firmware >= 1.0.7 (use files from directory 1.0.7)
	Copy the file "term.prefs" to the "Config" subdirectory within the Term installation.

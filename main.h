// Copyright (c) 2022 - 2023 by Rainer Wahler, RWahler@gmx.net

#ifndef MAIN_H
	#define	MAIN_H

	#define VERSION "1.0.12"

	#define BOOTLOADER_REQUESTED 0xEB

	// PORTA
	#define MOSI PIN1_bm
	#define MISO PIN2_bm
	#define SCK PIN3_bm
	#define MEM_RY_BY PIN4_bm
	#define MEM_RY_BY_CTRL PIN4CTRL
	#define STRB PIN5_bm
	#define LED2 PIN6_bm
	#define LED1 PIN7_bm

	// PORTB
	#define OE_D PIN0_bm
	#define MEM_WE PIN1_bm
	#define MEM_BYTE PIN4_bm
	#define MEM_RESET PIN5_bm

	// PORTC
	#define SH_LD PIN0_bm
	#define MEM_OE PIN1_bm
	#define MEM_CE PIN2_bm
	#define OE_A PIN3_bm

	#define CMD_READ_RESET_ADR1 0x00555
	#define CMD_READ_RESET_DATA1 0x00AA
	#define CMD_READ_RESET_ADR2 0x002AA
	#define CMD_READ_RESET_DATA2 0x0055
	#define CMD_READ_RESET_ADR3 0x00000
	#define CMD_READ_RESET_DATA3 0x00F0

	#define CMD_PROGRAM_ADR1 0x00555
	#define CMD_PROGRAM_DATA1 0x00AA
	#define CMD_PROGRAM_ADR2 0x002AA
	#define CMD_PROGRAM_DATA2 0x0055
	#define CMD_PROGRAM_ADR3 0x00555
	#define CMD_PROGRAM_DATA3 0x00A0

	#define CMD_CHIP_ERASE_ADR1 0x00555
	#define CMD_CHIP_ERASE_DATA1 0x00AA
	#define CMD_CHIP_ERASE_ADR2 0x002AA
	#define CMD_CHIP_ERASE_DATA2 0x0055
	#define CMD_CHIP_ERASE_ADR3 0x00555
	#define CMD_CHIP_ERASE_DATA3 0x0080
	#define CMD_CHIP_ERASE_ADR4 0x00555
	#define CMD_CHIP_ERASE_DATA4 0x00AA
	#define CMD_CHIP_ERASE_ADR5 0x002AA
	#define CMD_CHIP_ERASE_DATA5 0x0055
	#define CMD_CHIP_ERASE_ADR6 0x00555
	#define CMD_CHIP_ERASE_DATA6 0x0010

	#define CMD_BLOCK_ERASE_ADR1 0x00555
	#define CMD_BLOCK_ERASE_DATA1 0x00AA
	#define CMD_BLOCK_ERASE_ADR2 0x002AA
	#define CMD_BLOCK_ERASE_DATA2 0x0055
	#define CMD_BLOCK_ERASE_ADR3 0x00555
	#define CMD_BLOCK_ERASE_DATA3 0x0080
	#define CMD_BLOCK_ERASE_ADR4 0x00555
	#define CMD_BLOCK_ERASE_DATA4 0x00AA
	#define CMD_BLOCK_ERASE_ADR5 0x002AA
	#define CMD_BLOCK_ERASE_DATA5 0x0055
	#define CMD_BLOCK_ERASE_DATA6 0x0030	// Die Liste der Bl�cke (Adresse) innerhalb von maximal 50�s mit dem 6. Befehl hintereinander schicken

	#define CMD_CFI_QUERY_ADR1 0x00055
	#define CMD_CFI_QUERY_DATA1 0x0098

	#define CMD_CFI_DEVICE_QRY 0x0010		// 3 Adressen hintereinander "QRY"
	#define CMD_CFI_DEVICE_SIZE 0x0027		// 2^Wert = Gr��e in Bytes
	#define CMD_CFI_DEVICE_ID 0x0061		// 4 Adressen hintereinander (Low zu High Bytes)

	#define CFI_DEVICE_SIZE_512KB 0x0013
	#define CFI_DEVICE_SIZE_1024KB 0x0014
	#define CFI_DEVICE_SIZE_2048KB 0x0015

	#define FLASH_WAIT_ERROR 0x02000

	#define CHIP_TYPE_27C400 0
	#define CHIP_TYPE_FLASH_FT 1
	#define CHIP_TYPE_FLASH_BT 2

	#define CHIP_SIZE_UNKNOWN 0
	#define CHIP_SIZE_512KB 1
	#define CHIP_SIZE_1024KB 2
	#define CHIP_SIZE_2048KB 3

	#define TIMEOUT_BLOCK_ERASE (10 * 2)
	#define TIMEOUT_CHIP_ERASE (30 * 2)

	#define FLASH_BLOCK_SIZE 262144

	// Anzahl Schleifendurchg�nge die maximal gewartet werden soll
	#define COUNTER_WAIT_LOOPS 30
	// Millisekunden Verz�gerung zwschen der Ausgabe der Startzeichen
	#define COUNTER_DELAY 1000
	#define COUNTER_CHAR '$'

	// Adresse im EEProm wo die aktuell gew�hlte Geschwindigkeit gespeichert wird
	#define EEPROM_BPS_ADR 0x00
	#define EEPROM_BPS_921600 1
	#define EEPROM_BPS_460800 2
	#define EEPROM_BPS_230400 3
	#define EEPROM_BPS_115200 4
	#define EEPROM_BPS_57600 5
	#define EEPROM_BPS_38400 6
	#define EEPROM_BPS_19200 7
	#define EEPROM_BPS_9600 8

	#define EEPROM_LANG_ADR 0x01
	#define EEPROM_LANG_EN 1
	#define EEPROM_LANG_DE 2

	#define EEPROM_FLASH_RD_COUNT_ADR 0x02
	#define EEPROM_FLASH_WR_COUNT_ADR 0x04

#endif	/* MAIN_H */

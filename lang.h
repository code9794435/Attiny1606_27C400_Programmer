// Copyright (c) 2022 - 2023 by Rainer Wahler, RWahler@gmx.net

#ifndef LANG_H
	#define LANG_H

//***************************************************************************************
	// Allgemeine Spracheintraege, welche nicht uebersetzt werden

	// Interne Befehle
	#define CMD_BOOTLOADER "boot"
	#define CMD_RESET "reset"
	#define CMD_VERSION "version"

	// Anwender Befehle
	#define CMD_HELP "help"
	#define CMD_CHIP_TYPE "set_type"
	#define CMD_CHIP_INFO "identify"
	#define CMD_READ_ADDR "rd_addr"
	#define CMD_WRITE_ADDR "wr_addr"
	#define CMD_READ_BLOCK "rd_block"
	#define CMD_WRITE_BLOCK "wr_block"
	#define CMD_ERASE_BLOCK "del_block"
	#define CMD_ERASE_CHIP "del_chip"
	#define CMD_CHECK_BLOCK "chk_block"
	#define CMD_CHECK_CHIP "chk_chip"
	#define CMD_TEST_CHIP "test_chip"
	#define CMD_SERIAL_BPS "set_bps"
	#define CMD_LANGUAGE "set_lang"

	#define TEXT_COPYRIGHT "Programmed in Licence by Alinea Computer www.amiga-shop.net\r\n(c) by Rainer Wahler (RWahler@gmx.net)\r\n"

	#define TEXT_SERIAL_BPS_921600 "921600"
	#define TEXT_SERIAL_BPS_460800 "460800"
	#define TEXT_SERIAL_BPS_230400 "230400"
	#define TEXT_SERIAL_BPS_115200 "115200"
	#define TEXT_SERIAL_BPS_57600 "57600"
	#define TEXT_SERIAL_BPS_38400 "38400"
	#define TEXT_SERIAL_BPS_19200 "19200"
	#define TEXT_SERIAL_BPS_9600 "9600"

	#define TEXT_SIZE_512K "512KB"
	#define TEXT_SIZE_1024K "1024KB"
	#define TEXT_SIZE_2048K "2048KB"
	#define TEXT_CHIP_27C400 "27C400"
	#define TEXT_CHIP_FLASH_BT "Flash BT"
	#define TEXT_CHIP_FLASH_FT "Flash FT"

	#define TEXT_NEWLINE "\r\n"

	#define TEXT_OK "OK\r\n"

	#define TEXT_RESET "Reset\r\n"

	#define TEXT_CMD_VERSION "Version: "

	#define TEXT_ADDR_RANGE_1024K "7FFFF"
	#define TEXT_ADDR_RANGE_2048K "FFFFF"
	#define TEXT_ADDR_RANGE_512K "3FFFF"

	#define TEXT_BLOCK_SELECT2 "): "

	#define TEXT_CFI_QUERY_ID "CFI ID: "
	#define TEXT_CFI_CHIP_ID "\r\nChip ID: "
	#define TEXT_CFI_QUERY_ID_STRING "QRY"

	#define TEXT_CMD_HELP2 "'\r\n"

	#define TEXT_GET_ADDR2 "): "

	#define TEXT_UART_FRAME "[Err] Frame\r\n"

	#define TEXT_SERIAL_BPS_OPTIONS "\r\n1=921600\r\n2=460800\r\n3=230400\r\n4=115200\r\n5=57600\r\n6=38400\r\n7=19200\r\n8=9600\r\n"

//***************************************************************************************
	// Englische Spracheintraege

	#define TEXT_HELP_CMD_HELP_EN "=Help\r\n"
	#define TEXT_HELP_CMD_CHIP_INFO_EN "=Ident chip\r\n"
	#define TEXT_HELP_CMD_READ_ADDR_EN "=Read addr\r\n"
	#define TEXT_HELP_CMD_WRITE_ADDR_EN "=Write addr\r\n"
	#define TEXT_HELP_CMD_READ_BLOCK_EN "=Read block\r\n"
	#define TEXT_HELP_CMD_WRITE_BLOCK_EN "=Write block\r\n"
	#define TEXT_HELP_CMD_ERASE_BLOCK_EN "=Erase block\r\n"
	#define TEXT_HELP_CMD_ERASE_CHIP_EN "=Erase chip\r\n"
	#define TEXT_HELP_CMD_CHECK_BLOCK_EN "=Check block\r\n"
	#define TEXT_HELP_CMD_CHECK_CHIP_EN "=Check chip\r\n"
	#define TEXT_HELP_CMD_TEST_CHIP_EN "=Auto test\r\n"
	#define TEXT_HELP_CMD_SERIAL_BPS_EN "=Serial speed\r\n"
	#define TEXT_HELP_CMD_LANG_EN "=Lang\r\n"

	#define TEXT_FAILED_EN "Failed\r\n"
	#define TEXT_ABORT_EN "Abort\r\n"
	#define TEXT_TRUE_EN "True\r\n"
	#define TEXT_FALSE_EN "False\r\n"
	#define TEXT_SIZE_UNKNOWN_EN "unknown"

	#define TEXT_ADDR_MAX1_EN "Max addr = 0x"
	#define TEXT_ADDR_MAX2_EN "FFFF! -> Abort\r\n"

	#define TEXT_BLOCK_CHECK_EN "Check if block is empty: "

	#define TEXT_BLOCK_ERASE_OK_EN "Block erased!\r\n"
	#define TEXT_BLOCK_ERASE_ERR_EN "[ERR] Block not erased\r\n"
	#define TEXT_BLOCK_ERASE_ASK_EN "Erase block? (y/n) "
	#define TEXT_BLOCK_ERASE_MODE_EN "Block erase mode\r\n"

	#define TEXT_BLOCK_RANGE_EN "\r\nValid block number: 0-"

	#define TEXT_BLOCK_RD_OK_EN "\r\nSuccessfully trasmitted"
	#define TEXT_BLOCK_RD_ERR_EN "\r\n[ERR] Reading/Transmitting failed: "
	#define TEXT_BLOCK_RD_MODE_EN "Block read mode\r\n"
	#define TEXT_BLOCK_RD_START_EN "Block will be sended with X-Modem\r\n"

	#define TEXT_BLOCK_SELECT1_EN "Select block number (0-"

	#define TEXT_BLOCK_WR_OK_EN "Data successfully written\r\n"
	#define TEXT_BLOCK_WR_ERR_EN "\r\n[ERR] Receiving/Writing failed: "
	#define TEXT_BLOCK_WR_MODE_EN "Block write mode\r\n"
	#define TEXT_BLOCK_WR_START_EN "\r\nStart transmission with X-Modem!\r\n"
	#define TEXT_BLOCK_WR_SWAP_ASK_EN "File format BIN or ROM?: (b/r)"

	#define TEXT_BOOTLOADER_EN "Start Bootloader\r\n"

	#define TEXT_CFI_END_ERR_EN "[ERR] Flash read mode error"
	#define TEXT_CFI_QUERY_ID_ERR_EN "invalid"
	#define TEXT_CFI_SIZE_EN "\r\nSize: "
	#define TEXT_CFI_SIZE_UNKNOWN_EN "unknown 0x"
	#define TEXT_CFI_START_ERR_EN "[ERR] Flash CFI mode error\r\n"
	#define TEXT_CFI_TYPE_EN "\r\nType: "

	#define TEXT_CHIP_EMPTY_EN "Check if chip is empty: "

	#define TEXT_CHIP_ERASE_ERR_EN "[ERR] Chip could not be erased\r\n"
	#define TEXT_CHIP_ERASE_OK_EN "Chip erased!\r\n"
	#define TEXT_CHIP_ERASE_ASK_EN "Erase chip? (y/n) "

	#define TEXT_CHIP_TEST_ASK_EN "Start auto test (WIPE CHIP)? (y/n):"
	#define TEXT_CHIP_TEST_ERASE_EN ". Erase chip: "
	#define TEXT_CHIP_TEST_ERASE2_EN "Erase chip: "
	#define TEXT_CHIP_TEST_RESULT_EN "\r\nResult: "
	#define TEXT_CHIP_TEST_VERIFY_EN ". Verify data: "
	#define TEXT_CHIP_TEST_VERIFY_ERR1_EN "failed -> Addr: 0x"
	#define TEXT_CHIP_TEST_VERIFY_ERR2_EN " Data: 0x"
	#define TEXT_CHIP_TEST_WR1_EN ". Writing 0x"
	#define TEXT_CHIP_TEST_WR2_EN " into chip: "

	#define TEXT_CMD_ERR_EN "[Err] Unknown Cmd: "

	#define TEXT_CMD_HELP1_EN "Show help with '"

	#define TEXT_GET_ADDR1_EN "Addr in HEX number (0 - "

	#define TEXT_IDENTIFY_EN "First identify the chip\r\n"

	#define TEXT_NOT_SUPPORTED_EN "EProm not supported\r\n"

	#define TEXT_PROGRAM_START_EN "Starting Programmer for 27C400 Emulator\r\n"

	#define TEXT_RD_ADDR_ENDIAN_EN "Read value (little endian): 0x"

	#define TEXT_SHOW_CHIP_EN "Selected chip type="
	#define TEXT_SHOW_CHIP_SIZE_EN "\r\nChip size="

	#define TEXT_UART_HW_BUFFER_EN "[Err] HW Buffer\r\n"
	#define TEXT_UART_VAL_TOO_LONG_EN "[Err] Value too long\r\n"

	#define TEXT_WR_ADDR_EMPTY_EN "Check if addr is empty: "
	#define TEXT_WR_ADDR_EMPTY_ERR1_EN "false -> Addr not empty: 0x"
	#define TEXT_WR_ADDR_EMPTY_ERR2_EN " -> Abort\r\n"
	#define TEXT_WR_DATA_VALUE_EN "Value (0 - FFFF): "
	#define TEXT_WR_DATA_ASK_EN "Write data (y/n)?"
	#define TEXT_WR_VERIFY_EN "Verify data: "
	#define TEXT_WR_VERIFY_ERR_EN "failed -> Value is different: 0x"

	#define TEXT_BLOCK_START_TX_EN "Start the reception...\r\n"

	#define TEXT_SERIAL_BPS_ASK_EN "Select new speed:"
	#define TEXT_SERIAL_BPS_REBOOT_EN "Speed will be switched\r\n"
	#define TEXT_SERIAL_BPS_SHOW_EN "Speed (Bps)="

	#define TEXT_LANGUAGE_ASK_EN "Lang (1=EN, 2=DE)? "
	#define TEXT_LANGUAGE_OK_EN "Lang changed\r\n"

	#define TEXT_FLASH_COUNT_RD_EN "Total read: 0x"
	#define TEXT_FLASH_COUNT_WR_EN "Total write: 0x"

//***************************************************************************************
	// Deutsche Spracheintraege

	#define TEXT_HELP_CMD_HELP_DE "=Hilfe\r\n"
	#define TEXT_HELP_CMD_CHIP_INFO_DE "=Chip ident\r\n"
	#define TEXT_HELP_CMD_READ_ADDR_DE "=Lese Adresse\r\n"
	#define TEXT_HELP_CMD_WRITE_ADDR_DE "=Schreibe Adresse\r\n"
	#define TEXT_HELP_CMD_READ_BLOCK_DE "=Lese Block\r\n"
	#define TEXT_HELP_CMD_WRITE_BLOCK_DE "=Schreibe Block\r\n"
	#define TEXT_HELP_CMD_ERASE_BLOCK_DE "=Loesche Block\r\n"
	#define TEXT_HELP_CMD_ERASE_CHIP_DE "=Loesche Chip\r\n"
	#define TEXT_HELP_CMD_CHECK_BLOCK_DE "=Pruefe Block\r\n"
	#define TEXT_HELP_CMD_CHECK_CHIP_DE "=Pruefe Chip\r\n"
	#define TEXT_HELP_CMD_TEST_CHIP_DE "=Auto Test\r\n"
	#define TEXT_HELP_CMD_SERIAL_BPS_DE "=Serielle Geschwindigkeit\r\n"
	#define TEXT_HELP_CMD_LANG_DE "=Sprache\r\n"

	#define TEXT_FAILED_DE "Fehlgeschlagen\r\n"
	#define TEXT_ABORT_DE "Abbruch\r\n"
	#define TEXT_TRUE_DE "Wahr\r\n"
	#define TEXT_FALSE_DE "Falsch\r\n"
	#define TEXT_SIZE_UNKNOWN_DE "unbekannt"

	#define TEXT_ADDR_MAX1_DE "Max Adresse = 0x"
	#define TEXT_ADDR_MAX2_DE "FFFF! -> Abbruch\r\n"

	#define TEXT_BLOCK_CHECK_DE "Pruefe ob der Block leer ist: "

	#define TEXT_BLOCK_ERASE_OK_DE "Block geloescht!\r\n"
	#define TEXT_BLOCK_ERASE_ERR_DE "[ERR] Block konnte nicht geloescht werden\r\n"
	#define TEXT_BLOCK_ERASE_ASK_DE "Block loeschen? (y/n) "
	#define TEXT_BLOCK_ERASE_MODE_DE "Block Loeschen Modus\r\n"

	#define TEXT_BLOCK_RANGE_DE "\r\nGueltige Blocknummer: 0-"

	#define TEXT_BLOCK_RD_OK_DE "\r\nErfolgreich uebertragen"
	#define TEXT_BLOCK_RD_ERR_DE "\r\n[ERR] Lesen/Uebertragen fehlgeschlagen: "
	#define TEXT_BLOCK_RD_MODE_DE "Block Lesen Modus\r\n"
	#define TEXT_BLOCK_RD_START_DE "Der Block wird per X-Modem gesendet\r\n"

	#define TEXT_BLOCK_SELECT1_DE "Block auswaehlen (0-"

	#define TEXT_BLOCK_WR_OK_DE "Daten erfolgreich geschrieben\r\n"
	#define TEXT_BLOCK_WR_ERR_DE "\r\n[ERR] Empfangen/Schreiben fehlgeschlagen: "
	#define TEXT_BLOCK_WR_MODE_DE "Block Schreiben Modus\r\n"
	#define TEXT_BLOCK_WR_START_DE "\r\nStarten Sie die Uebertragung per X-Modem!\r\n"
	#define TEXT_BLOCK_WR_SWAP_ASK_DE "Dateiformat BIN oder ROM?: (b/r) "

	#define TEXT_BOOTLOADER_DE "Starte Bootloader\r\n"

	#define TEXT_CFI_END_ERR_DE "[ERR] Flash Lesemodus Fehler"
	#define TEXT_CFI_QUERY_ID_ERR_DE "ungueltig"
	#define TEXT_CFI_SIZE_DE "\r\nGroesse: "
	#define TEXT_CFI_SIZE_UNKNOWN_DE "unkekannt 0x"
	#define TEXT_CFI_START_ERR_DE "[ERR] Flash CFI Modus Fehler\r\n"
	#define TEXT_CFI_TYPE_DE "\r\nTyp: "

	#define TEXT_CHIP_EMPTY_DE "Pruefe ob der Chip leer ist: "

	#define TEXT_CHIP_ERASE_ERR_DE "[ERR] Chip konnte nicht geloescht werden\r\n"
	#define TEXT_CHIP_ERASE_OK_DE "Chip geloescht!\r\n"
	#define TEXT_CHIP_ERASE_ASK_DE "Loesche Chip? (y/n) "

	#define TEXT_CHIP_TEST_ASK_DE "Starte auto Test (ALLES WIRD GELOESCHT)? (y/n):"
	#define TEXT_CHIP_TEST_ERASE_DE ". Loesche Chip: "
	#define TEXT_CHIP_TEST_ERASE2_DE "Loesche Chip: "
	#define TEXT_CHIP_TEST_RESULT_DE "\r\nErgebnis: "
	#define TEXT_CHIP_TEST_VERIFY_DE ". Verifiziere Daten: "
	#define TEXT_CHIP_TEST_VERIFY_ERR1_DE "Fehler -> Adresse: 0x"
	#define TEXT_CHIP_TEST_VERIFY_ERR2_DE " Daten: 0x"
	#define TEXT_CHIP_TEST_WR1_DE ". Schreibe 0x"
	#define TEXT_CHIP_TEST_WR2_DE " in den Chip: "

	#define TEXT_CMD_ERR_DE "[Err] Unbekannter Befehl: "

	#define TEXT_CMD_HELP1_DE "Hilfe mit '"

	#define TEXT_GET_ADDR1_DE "Adresse in HEX Zahl (0 - "

	#define TEXT_IDENTIFY_DE "Erst Chip identifizieren\r\n"

	#define TEXT_NOT_SUPPORTED_DE "EProms werden nicht unterstuetzt\r\n"

	#define TEXT_PROGRAM_START_DE "Starte Programmiergeraet fuer 27C400 Emulator\r\n"

	#define TEXT_RD_ADDR_ENDIAN_DE "Lese Wert (little endian): 0x"

	#define TEXT_SHOW_CHIP_DE "Ausgewaehlter Chip Typ="
	#define TEXT_SHOW_CHIP_SIZE_DE "\r\nChip Groessee="

	#define TEXT_UART_HW_BUFFER_DE "[Err] HW Puffer\r\n"
	#define TEXT_UART_VAL_TOO_LONG_DE "[Err] Wert zu lang\r\n"

	#define TEXT_WR_ADDR_EMPTY_DE "Pruefe ob die Adresse leer ist: "
	#define TEXT_WR_ADDR_EMPTY_ERR1_DE "Fehler -> Adresse ist nicht leer: 0x"
	#define TEXT_WR_ADDR_EMPTY_ERR2_DE " -> Abbruch\r\n"
	#define TEXT_WR_DATA_VALUE_DE "Wert (0 - FFFF): "
	#define TEXT_WR_DATA_ASK_DE "Schreibe Daten (y/n)?"
	#define TEXT_WR_VERIFY_DE "Verifiziere Daten: "
	#define TEXT_WR_VERIFY_ERR_DE "Fehler -> Wert ist unterschiedlich: 0x"

	#define TEXT_BLOCK_START_TX_DE "Starten Sie den Empfang...\r\n"

	#define TEXT_SERIAL_BPS_ASK_DE "Waehlen Sie die neue Geschwindigkeit:"
	#define TEXT_SERIAL_BPS_REBOOT_DE "Geschwindigkeit wird umgeschaltet\r\n"
	#define TEXT_SERIAL_BPS_SHOW_DE "Geschwindigkeit (Bps)="

	#define TEXT_LANGUAGE_ASK_DE "Sprache (1=EN, 2=DE)? "
	#define TEXT_LANGUAGE_OK_DE "Sprache umgestellt\r\n"

	#define TEXT_FLASH_COUNT_RD_DE "Insgesamt Lesen: 0x"
	#define TEXT_FLASH_COUNT_WR_DE "Insgesamt Schreiben: 0x"
#endif

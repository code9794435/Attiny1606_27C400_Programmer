// Copyright (c) 2022 - 2023 by Rainer Wahler, RWahler@gmx.nen

#include "main.h"
#include "commonHelpers.h"
#include "lang.h"
#include "../Library.X/libUART.h"
#include "../Library.X/libConvert.h"
#include "../Library.X/libXYModem.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/cpufunc.h>
#include <util/delay.h>
#include <stdlib.h>
#include <string.h>

const uint32_t u32_FLASH_BLOCK0_2048KB_BT[] = {0x00000, 0x02000, 0x03000, 0x04000, 0x08000, 0x10000, 0x18000, 0x20000, 0x28000, 0x30000, 0x38000};
const uint32_t u32_FLASH_BLOCK0_2048KB_FT[] = {0x00000, 0x08000, 0x10000, 0x18000, 0x20000, 0x28000, 0x30000, 0x38000};
const uint32_t u32_FLASH_BLOCK1_2048KB[] = {0x40000, 0x48000, 0x50000, 0x58000, 0x60000, 0x68000, 0x70000, 0x78000};
const uint32_t u32_FLASH_BLOCK2_2048KB[] = {0x80000, 0x88000, 0x90000, 0x98000, 0xA0000, 0xA8000, 0xB0000, 0xB8000};
const uint32_t u32_FLASH_BLOCK3_2048KB_BT[] = {0xC0000, 0xC8000, 0xD0000, 0xD8000, 0xE0000, 0xE8000, 0xF0000, 0xF8000};
const uint32_t u32_FLASH_BLOCK3_2048KB_FT[] = {0xC0000, 0xC8000, 0xD0000, 0xD8000, 0xE0000, 0xE8000, 0xF0000, 0xF8000, 0xFC000, 0xFD000, 0xFE000};

const uint32_t u32_FLASH_BLOCK0_1024KB_BT[] = {0x00000, 0x02000, 0x03000, 0x04000, 0x08000, 0x10000, 0x18000, 0x20000, 0x28000, 0x30000, 0x38000};
const uint32_t u32_FLASH_BLOCK0_1024KB_FT[] = {0x00000, 0x08000, 0x10000, 0x18000, 0x20000, 0x28000, 0x30000, 0x38000};
const uint32_t u32_FLASH_BLOCK1_1024KB_BT[] = {0x40000, 0x48000, 0x50000, 0x58000, 0x60000, 0x68000, 0x70000, 0x78000};
const uint32_t u32_FLASH_BLOCK1_1024KB_FT[] = {0x40000, 0x48000, 0x50000, 0x58000, 0x60000, 0x68000, 0x70000, 0x78000, 0x7C000, 0x7D000, 0x7E000};

const uint32_t u32_FLASH_BLOCK0_512KB_BT[] = {0x00000, 0x02000, 0x03000, 0x04000, 0x08000, 0x10000, 0x18000, 0x20000, 0x28000, 0x30000, 0x38000};
const uint32_t u32_FLASH_BLOCK0_512KB_FT[] = {0x00000, 0x08000, 0x10000, 0x18000, 0x20000, 0x28000, 0x30000, 0x38000, 0x3C000, 0x3D000, 0x3E000};

// 512KB, 1024KB, 2048KB
uint8_t u8_ChipSize = CHIP_SIZE_512KB;
// 27C400, Flash-FB, Flash-FT
uint8_t u8_ChipType = CHIP_TYPE_27C400;
// Anzahl erfolgreicher Lese-/Schreibvorg�nge
uint16_t u16_CountReadCycles, u16_CountWriteCycles;
// Sprache der seriellen Ausgaben
bool b_LangEn = true;

uint16_t u16_GetUsartBps();
void v_getLanguage();
void v_getFlashReadWriteCount();
void v_setFlashReadCount();
void v_setFlashWriteCount();

/**
 * Initialisierung
 */
void v_init(void) {
	// CPU Prescaler (/6) deaktivieren, wodurch die CPU direkt mit 20MHz l�uft
	ccp_write_io((void*)&CLKCTRL.MCLKCTRLB, ~CLKCTRL_PEN_bm);

	// Port konfigurieren
	// MOSI
	PORTA.DIRSET = MOSI;
	// MISO
	PORTA.DIRCLR = MISO;
	// SCK
	PORTA.DIRSET = SCK;
	// MEM.RY/!BY
	PORTA.DIRCLR = MEM_RY_BY;
	PORTA.MEM_RY_BY_CTRL |= PORT_PULLUPEN_bm;
	// STRB
	PORTA.DIRSET = STRB;
	// LED2
	PORTA.DIRSET = LED2;
	// LED1
	PORTA.DIRSET = LED1;

	// !OE_D
	PORTB.DIRSET = OE_D;
	// MEM.!WE
	PORTB.DIRSET = MEM_WE;
	// MEM.!BYTE
	PORTB.DIRSET = MEM_BYTE;
	// MEM.!RESET
	PORTB.DIRSET = MEM_RESET;

	// SH_!LD
	PORTC.DIRSET = SH_LD;
	// MEM.!OE
	PORTC.DIRSET = MEM_OE;
	// MEM.!CE
	PORTC.DIRSET = MEM_CE;
	// !OE_A
	PORTC.DIRSET = OE_A;

	// STRB auf 0 setzen
	PORTA.OUTCLR = STRB;

	// MEM.!WE auf 1 setzen
	PORTB.OUTSET = MEM_WE;
	// MEM.!BYTE auf 1 setzen
	PORTB.OUTSET = MEM_BYTE;
	// !OE_D auf 1 setzen
	PORTB.OUTSET = OE_D;
	// MEM.!RESET auf 1 setzen
	PORTB.OUTSET = MEM_RESET;

	// !OE_A auf 1 setzen
	PORTC.OUTSET = OE_A;
	// MEM.!OE auf 1 setzen
	PORTC.OUTSET = MEM_OE;
	// MEM.!CE auf 1 setzen
	PORTC.OUTSET = MEM_CE;
	// SH_!LD auf 1 setzen
	PORTC.OUTSET = SH_LD;

	// MSB first, SPI Master, Doppelter Takt, Prescaler 4
	// DORD=0, MASTER=1, CLK2X=1, PRESC=DIV4, ENABLE=0
	SPI0.CTRLA = SPI_MASTER_bm | SPI_CLK2X_bm | SPI_PRESC_DIV4_gc;

	// SS Leitung nicht verwenden, SPI Mode 0
	// BUFEN=0, BUFWR=0, SSD=1, MODE=0
	SPI0.CTRLB = SPI_SSD_bm;

	// SPI aktivieren
	SPI0.CTRLA |= SPI_ENABLE_bm;

	v_UartInit(&SERIAL_TINY160X_PORT, SERIAL_TINY160X_TXD, SERIAL_TINY160X_RXD, u16_GetUsartBps());
	v_UartBackspaceSupport(true);
	v_getLanguage();
	v_getFlashReadWriteCount();
}

/**
 * 16Bit Daten und 20Bit Adresse �bernehmen
 */
void v_SetAddressAndDataToOutput() {
	// STRB = 1
	PORTA.OUTSET = STRB;
	// STRB = 0
	PORTA.OUTCLR = STRB;
}

/**
 * 16Bit Daten an den Schieberegistern einlesen
 */
void v_SetDataInputStore() {
	// SH_!LD = 0
	PORTC.OUTCLR = SH_LD;
	// SH_!LD = 1
	PORTC.OUTSET = SH_LD;
}

/**
 * Ausg�nge der Schieberegister f�r die 20Bit Adresse deaktivieren
 */
void v_SetShiftAddressOutputOff() {
	// !OE_A = 1
	PORTC.OUTSET = OE_A;
}

/**
 * Ausg�nge der Schieberegister f�r die 20Bit Adresse aktivieren
 */
void v_SetShiftAddressOutputOn() {
	// !OE_A = 0
	PORTC.OUTCLR = OE_A;
}

/**
 * Ausg�nge der Schieberegister f�r die 16Bit Daten deaktivieren
 */
void v_SetShiftDataOutputOff() {
	// !OE_D = 1
	PORTB.OUTSET = OE_D;
}

/**
 * Ausg�nge der Schieberegister f�r die 16Bit Daten aktivieren
 */
void v_SetShiftDataOutputOn() {
	// !OE_D = 0
	PORTB.OUTCLR = OE_D;
}

/**
 * LED1 ausschalten
 */
void v_SetLedOff_1() {
	PORTA.OUTSET = LED1;
}

/**
 * LED1 einschalten
 */
void v_SetLedOn_1() {
	PORTA.OUTCLR = LED1;
}

/**
 * LED2 ausschalten
 */
void v_SetLedOff_2() {
	PORTA.OUTSET = LED2;
}

/**
 * LED2 einschalten
 */
void v_SetLedOn_2() {
	PORTA.OUTCLR = LED2;
}

/**
 * !OE ds Chips deaktivieren
 */
void v_SetMemOutputEnableOff() {
	// MEM.!OE = 1
	PORTC.OUTSET = MEM_OE;
}

/**
 * !OE ds Chips aktivieren
 */
void v_SetMemOutputEnableOn() {
	// MEM.!OE = 0
	PORTC.OUTCLR = MEM_OE;
}

/**
 * !CE des Chips deaktivieren
 */
void v_SetMemChipEnableOff() {
	// MEM.!CE = 1
	PORTC.OUTSET = MEM_CE;
}

/**
 * !CE des Chips aktivieren
 */
void v_SetMemChipEnableOn() {
	// MEM.!CE = 0
	PORTC.OUTCLR = MEM_CE;
}

/**
 * !WE ds Chips aktivieren
 */
void v_SetMemWriteEnableStore() {
	// MEM.!WE = 0
	PORTB.OUTCLR = MEM_WE;
	// MEM.!WE = 1
	PORTB.OUTSET = MEM_WE;
}

/**
 * 8Bit Daten Modus des Chips deaktivieren
 */
void v_SetMemByteOff() {
	// MEM.!BYTE = 1
	PORTB.OUTSET = MEM_BYTE;
}

/**
 * 8Bit Daten Modus des Chips aktivieren
 */
void v_SetMemByteOn() {
	// MEM.!BYTE = 0
	PORTB.OUTCLR = MEM_BYTE;
}

/**
 * Reset des Chips deaktivieren
 */
void v_SetMemResetOff() {
	// MEM.!Reset = 1
	PORTB.OUTSET = MEM_RESET;
}

/**
 * Reset des Chips aktivieren
 */
void v_SetMemResetOn() {
	// MEM.!Reset = 0
	PORTB.OUTCLR = MEM_RESET;
}

/**
 * Pr�fen ob der Flash gerade besch�ftigt ist
 * @return true falls besch�ftigt
 */
bool b_IsMemBusy() {
	if ((PORTA.IN & MEM_RY_BY) == 0)
		return true;
	return false;
}

/**
 * High- und Low-Byte einer 16Bit Zahl vertauschen
 * @param u16_Input 16Bit Zahl, welche verarbeitet werden soll
 * @return 16Bit Zahl mit vertauschten High-/Low-Bytes
 */
uint16_t u16_ChangeHighLowBytes(uint16_t u16_Input) {
	uint8_t u8_Temp;

	// High- und Low-Byte vertauschen damit es der Reihenfolge im 8Bit Modus entspricht
	u8_Temp = (uint8_t) (u16_Input >> 8) & 0xFF;
	u16_Input <<= 8;
	u16_Input |= u8_Temp;

	return u16_Input;
}

/**
 * 20Bit Adresse und 16Bit Daten �ber die Schieberegister ausgeben
 * @param u32_address 20Bit Adresse
 * @param u16_data 16Bit Daten
 * @param b_Address2Output true: Adresse wird an die Ausg�nge gelegt
 * @param b_Data2Output true: Daten werden an die Ausg�nge gelegt
 */
void v_ShiftOutData(uint32_t u32_address, uint16_t u16_data) {
	uint8_t u8_byte;

	// MSB Adresse
	u8_byte = (uint8_t) (u32_address >> 16);

	SPI0.DATA = u8_byte;

	u8_byte = (uint8_t) (u32_address >> 8);

	// Warten bis Daten in den SPI Sendepuffer geschrieben werden k�nnen
	while(!(SPI0.INTFLAGS & SPI_IF_bm));

	SPI0.DATA = u8_byte;

	// LSB Adresse
	u8_byte = (uint8_t) u32_address;

	// Warten bis Daten in den SPI Sendepuffer geschrieben werden k�nnen
	while(!(SPI0.INTFLAGS & SPI_IF_bm));

	SPI0.DATA = u8_byte;

	// MSB Daten
	u8_byte = (uint8_t) (u16_data >> 8);

	// Warten bis Daten in den SPI Sendepuffer geschrieben werden k�nnen
	while(!(SPI0.INTFLAGS & SPI_IF_bm));

	SPI0.DATA = u8_byte;

	// LSB Daten
	u8_byte = (uint8_t) u16_data;

	// Warten bis Daten in den SPI Sendepuffer geschrieben werden k�nnen
	while(!(SPI0.INTFLAGS & SPI_IF_bm));

	SPI0.DATA = u8_byte;

	// Warten bis alle Daten gesendet wurden
	while(!(SPI0.INTFLAGS & SPI_IF_bm));

	// Logik-Zust�nde an die Ausg�nge der Schieberegister �bernehmen
	v_SetAddressAndDataToOutput();
}

/**
 * 16Bit Daten �ber die Schieberegister einlesen
 * @return Eingelesene 16Bit Daten
 */
uint16_t u16_ShiftInData() {
	uint16_t u16_data = 0;

	// Daten �bernehmen
	v_SetDataInputStore();

	// Dummy Byte senden um ein Byte zu empfangen
	SPI0.DATA = 0;

	// Warten bis alle Daten gesendet wurden
	while(!(SPI0.INTFLAGS & SPI_IF_bm));

	// High-Byte
	u16_data = SPI0.DATA << 8;

	// Dummy Byte senden um ein Byte zu empfangen
	SPI0.DATA = 0;

	// Warten bis alle Daten gesendet wurden
	while(!(SPI0.INTFLAGS & SPI_IF_bm));

	// Low-Byte
	u16_data += SPI0.DATA;

	return u16_data;
}

/**
 * Ein 16Bit Wort aus einer Adresse auslesen (Als Little Endian im Flash gespeichert)
 * @param u32_StartAddress Adresse von welcher die Daten gelesen werden sollen
 * @return 16Bit Daten
 */
uint16_t u16_ReadMemoryAddress(uint32_t u32_StartAddress) {
	uint16_t u16_Data;

	v_SetMemChipEnableOn();
	v_SetMemOutputEnableOn();

	// Adresse ausgeben
	v_SetShiftDataOutputOff();
	v_ShiftOutData(u32_StartAddress, 0);

	// Daten einlesen
	u16_Data = u16_ShiftInData();
	v_SetMemChipEnableOff();

	return u16_Data;
}

/**
 * n Adressen aus dem Flash auslesen und die 16Bit Daten im Array eintragen
 * @param u8_Data Array welches die ausgelesenen 16Bit Daten (Low/High Byte) enthalten
 * @param u8_Length Anzahl Adressen, welche ausgelesen werden sollen
 * @param u32_StartAddress Start Adresse im 16Bit Modus (z.B. 4MBit = 256k x 16)
 */
void v_ReadMemoryDataArray(uint8_t *u8_Data, uint8_t u8_Length, uint32_t u32_StartAddress) {
	uint8_t u8_Counter = 0;
	uint16_t u16_Data;

	v_SetMemChipEnableOn();
	v_SetMemOutputEnableOn();
	v_SetShiftDataOutputOff();

	for (u8_Counter = 0; u8_Counter < u8_Length; u8_Counter++) {
		// Adresse ausgeben
		v_ShiftOutData(u32_StartAddress + u8_Counter, 0);
		// Daten einlesen
		u16_Data = u16_ShiftInData();

		// Low Byte
		u8_Data[2 * u8_Counter] = u16_Data & 0xFF;
		// High Byte
		u8_Data[2 * u8_Counter + 1] = u16_Data >> 8;
	}

	v_SetMemChipEnableOff();
}

/**
 * Warten bis der Flash bereit ist
 * @return true falls bereit
 */
bool b_WaitForMem() {
	uint8_t u8_Counter = 255;

	// Maximal 2550�s Warten bis der Speicher bereit ist
	while(b_IsMemBusy() && u8_Counter > 0) {
		_delay_us(10);
		u8_Counter--;
	}

	if (u8_Counter == 0)
		return false;

	return true;
}

/**
 * Read/Reset Befehl an Flash schicken
 * @return true falls alles geklappt hat
 */
bool b_WriteMemoryCmdReadReset() {
	if (b_WaitForMem() == false)
		return false;

	v_SetMemOutputEnableOff();
	v_SetShiftDataOutputOn();
	v_SetMemChipEnableOn();

	v_ShiftOutData(CMD_READ_RESET_ADR1, CMD_READ_RESET_DATA1);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_READ_RESET_ADR2, CMD_READ_RESET_DATA2);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_READ_RESET_ADR3, CMD_READ_RESET_DATA3);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_SetMemChipEnableOff();
	return true;
}

/**
 * Read CFI Query Befehl an Flash schicken
 * @return true falls alles geklappt hat
 */
bool b_WriteMemoryCmdCfiQuery() {
	if (b_WaitForMem() == false)
		return false;

	v_SetMemOutputEnableOff();
	v_SetShiftDataOutputOn();
	v_SetMemChipEnableOn();

	v_ShiftOutData(CMD_CFI_QUERY_ADR1, CMD_CFI_QUERY_DATA1);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_SetMemChipEnableOff();
	return true;
}

/**
 * 16Bit Daten in eine Adresse des Flash programmieren
 * @param u32_Address Adresse
 * @param u16_Data 16Bit Daten
 * @return true falls alles geklappt hat
 */
bool b_WriteMemoryData(uint32_t u32_Address, uint16_t u16_Data) {
	if (b_WaitForMem() == false)
		return false;

	v_SetMemOutputEnableOff();
	v_SetShiftDataOutputOn();
	v_SetMemChipEnableOn();

	v_ShiftOutData(CMD_PROGRAM_ADR1, CMD_PROGRAM_DATA1);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_PROGRAM_ADR2, CMD_PROGRAM_DATA2);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_PROGRAM_ADR3, CMD_PROGRAM_DATA3);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(u32_Address, u16_Data);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_SetMemChipEnableOff();
	return true;
}

/**
 * n Adressen in das Flash schreiben wobei die 16Bit Daten im Array stehen
 * @param u8_Data Array welches die zu schreibenden 16Bit Daten (Low/High Byte) enthalten
 * @param u8_Length Anzahl 16Bit Adressen, welche beschrieben werden sollen
 * @param u32_StartAddress Start Adresse im 16Bit Modus (z.B. 4MBit = 256k x 16)
 * @param b_byteSwap true: High-/Low-Byte werden vertauscht
 * @return true falls alles geklappt hat
 */
bool b_WriteMemoryDataArray(uint8_t *u8_Data, uint8_t u8_Length, uint32_t u32_StartAddress, bool b_byteSwap) {
	uint8_t u8_Counter = 0;
	uint16_t u16_Data;
	
	for (u8_Counter = 0; u8_Counter < u8_Length; u8_Counter++) {
		if (b_byteSwap) {
			// Low Byte
			u16_Data = u8_Data[2 * u8_Counter] << 8;
			// High Byte
			u16_Data += u8_Data[2 * u8_Counter + 1];
		}
		else {
			// High Byte
			u16_Data = u8_Data[2 * u8_Counter + 1] << 8;
			// Low Byte
			u16_Data += u8_Data[2 * u8_Counter];
		}

		if (!b_WriteMemoryData(u32_StartAddress + u8_Counter, u16_Data))
			return false;
	}

	return true;
}

/**
 * Flash vollst�ndig l�schen
 * @return true falls alles geklappt hat
 */
bool b_WriteMemoryChipErase() {
	uint8_t u8_Counter;

	if (b_WaitForMem() == false)
		return false;

	v_SetLedOn_2();
	v_SetMemOutputEnableOff();
	v_SetShiftDataOutputOn();
	v_SetMemChipEnableOn();

	v_ShiftOutData(CMD_CHIP_ERASE_ADR1, CMD_CHIP_ERASE_DATA1);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_CHIP_ERASE_ADR2, CMD_CHIP_ERASE_DATA2);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_CHIP_ERASE_ADR3, CMD_CHIP_ERASE_DATA3);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_CHIP_ERASE_ADR4, CMD_CHIP_ERASE_DATA4);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_CHIP_ERASE_ADR5, CMD_CHIP_ERASE_DATA5);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_CHIP_ERASE_ADR6, CMD_CHIP_ERASE_DATA6);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();
	_delay_ms(1);

	// Maximal 10s Warten bis der Speicher bereit ist
	u8_Counter = TIMEOUT_CHIP_ERASE;
	while(b_IsMemBusy() && u8_Counter > 0) {
		v_UartSendChar('.');
		_delay_ms(500);
		u8_Counter--;
	}

	v_SetMemChipEnableOff();
	v_SetLedOff_2();

	if (u8_Counter == 0)
		return false;
	return true;
}

/**
 * Einen Block im Flash l�schen
 * @param u8_BlockNum Blocknummer (0-3)
 * @return true falls alles geklappt hat
 */
bool b_WriteMemoryBlockErase(uint8_t u8_BlockNum) {
	uint8_t u8_Counter, u8_MaxBlock;
	uint32_t u32_BlockAddress;
	uint32_t const* u32p_BlockPtr;

	switch (u8_BlockNum) {
		case 0:
			if (u8_ChipSize == CHIP_SIZE_512KB) {
				if (u8_ChipType == CHIP_TYPE_FLASH_BT) {
					u32p_BlockPtr = u32_FLASH_BLOCK0_512KB_BT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK0_512KB_BT) / sizeof u32_FLASH_BLOCK0_512KB_BT[0];
				}
				else {
					u32p_BlockPtr = u32_FLASH_BLOCK0_512KB_FT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK0_512KB_FT) / sizeof u32_FLASH_BLOCK0_512KB_FT[0];
				}
			}
			else if (u8_ChipSize == CHIP_SIZE_1024KB) {
				if (u8_ChipType == CHIP_TYPE_FLASH_BT) {
					u32p_BlockPtr = u32_FLASH_BLOCK0_1024KB_BT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK0_1024KB_BT) / sizeof u32_FLASH_BLOCK0_1024KB_BT[0];
				}
				else {
					u32p_BlockPtr = u32_FLASH_BLOCK0_1024KB_FT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK0_1024KB_FT) / sizeof u32_FLASH_BLOCK0_1024KB_FT[0];
				}
			}
			else {
				if (u8_ChipType == CHIP_TYPE_FLASH_BT) {
					u32p_BlockPtr = u32_FLASH_BLOCK0_2048KB_BT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK0_2048KB_BT) / sizeof u32_FLASH_BLOCK0_2048KB_BT[0];
				}
				else {
					u32p_BlockPtr = u32_FLASH_BLOCK0_2048KB_FT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK0_2048KB_FT) / sizeof u32_FLASH_BLOCK0_2048KB_FT[0];
				}
			}
			break;
		case 1:
			if (u8_ChipSize == CHIP_SIZE_512KB)
				return false;
			else if (u8_ChipSize == CHIP_SIZE_1024KB) {
				if (u8_ChipType == CHIP_TYPE_FLASH_BT) {
					u32p_BlockPtr = u32_FLASH_BLOCK1_1024KB_BT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK1_1024KB_BT) / sizeof u32_FLASH_BLOCK1_1024KB_BT[0];
				}
				else {
					u32p_BlockPtr = u32_FLASH_BLOCK1_1024KB_FT;
					u8_MaxBlock = sizeof(u32_FLASH_BLOCK1_1024KB_FT) / sizeof u32_FLASH_BLOCK1_1024KB_FT[0];
				}
			}
			else {
				u32p_BlockPtr = u32_FLASH_BLOCK1_2048KB;
				u8_MaxBlock = sizeof(u32_FLASH_BLOCK1_2048KB) / sizeof u32_FLASH_BLOCK1_2048KB[0];
			}
			break;
		case 2:
			if (u8_ChipSize == CHIP_SIZE_512KB || u8_ChipSize == CHIP_SIZE_1024KB)
				return false;

			u32p_BlockPtr = u32_FLASH_BLOCK2_2048KB;
			u8_MaxBlock = sizeof(u32_FLASH_BLOCK2_2048KB) / sizeof u32_FLASH_BLOCK2_2048KB[0];
			break;
		case 3:
			if (u8_ChipSize == CHIP_SIZE_512KB || u8_ChipSize == CHIP_SIZE_1024KB)
				return false;

			if (u8_ChipType == CHIP_TYPE_FLASH_BT) {
				u32p_BlockPtr = u32_FLASH_BLOCK3_2048KB_BT;
				u8_MaxBlock = sizeof(u32_FLASH_BLOCK3_2048KB_BT) / sizeof u32_FLASH_BLOCK3_2048KB_BT[0];
			}
			else {
				u32p_BlockPtr = u32_FLASH_BLOCK3_2048KB_FT;
				u8_MaxBlock = sizeof(u32_FLASH_BLOCK3_2048KB_FT) / sizeof u32_FLASH_BLOCK3_2048KB_FT[0];
			}
			break;
		default:
			// Ung�ltiger Block
			return false;
	}

	v_SetLedOn_2();
	if (b_WaitForMem() == false)
		return false;

	v_SetMemOutputEnableOff();
	v_SetShiftDataOutputOn();
	v_SetMemChipEnableOn();

	v_ShiftOutData(CMD_BLOCK_ERASE_ADR1, CMD_BLOCK_ERASE_DATA1);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_BLOCK_ERASE_ADR2, CMD_BLOCK_ERASE_DATA2);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_BLOCK_ERASE_ADR3, CMD_BLOCK_ERASE_DATA3);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_BLOCK_ERASE_ADR4, CMD_BLOCK_ERASE_DATA4);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	v_ShiftOutData(CMD_BLOCK_ERASE_ADR5, CMD_BLOCK_ERASE_DATA5);
	// Daten �bernehmen
	v_SetMemWriteEnableStore();

	for (u8_Counter = 0; u8_Counter < u8_MaxBlock; u8_Counter++) {
		u32_BlockAddress = u32p_BlockPtr[u8_Counter];
		v_ShiftOutData(u32_BlockAddress, CMD_BLOCK_ERASE_DATA6);
		// Daten �bernehmen
		v_SetMemWriteEnableStore();
	}

	_delay_ms(1);

	// Maximal 10s Warten bis der Speicher bereit ist
	u8_Counter = TIMEOUT_BLOCK_ERASE;
	while(b_IsMemBusy() && u8_Counter > 0) {
		v_UartSendChar('.');
		_delay_ms(500);
		u8_Counter--;
	}
	
	v_SetMemChipEnableOff();
	v_SetLedOff_2();

	if (u8_Counter == 0)
		return false;
	return true;
}

/**
 *  Text in der eingestellten Sprache ausgeben
 * pc_TextEn = Pointer zum englischen Text
 * pc_TextDe = Pointer zum deutschen Text
 */
void v_SendStringTranslated(char *pc_TextEn, char *pc_TextDe) {
	if (b_LangEn)
		// Englisch
		v_UartSendString(pc_TextEn);
	else
		// Deutsch
		v_UartSendString(pc_TextDe);
}

/**
 * Maximal 30s lang ein Zeichen ausgeben solange auf ein \n gewartet wird
 */
void v_DelayCounter() {
	uint8_t u8_Counter;

	v_UartClearRxBuffer();
	for (u8_Counter = 0; u8_Counter < COUNTER_WAIT_LOOPS; u8_Counter++) {
		_delay_ms(COUNTER_DELAY);

		if (b_UartIsNewlineDetected())
			// Sobald ein \n empfangen wird die Warteschleife abbrechen
			break;

		v_UartSendChar(COUNTER_CHAR);
	}

	v_UartSendString(TEXT_NEWLINE);
}

/**
 * Chip auslesen und Daten aus einem Block �ber das X-Modem Protokoll verschicken
 * @param u8_BlockNum Blocknummer, welche gelesen werden soll (0-3)
 * @return Fehlermeldungen
 */
uint8_t u8_TransmitBlock(uint8_t u8_BlockNum) {
	uint8_t u8_Status, u8_DataSize, u8_BlockData[128], u8_Error = 0;
	uint32_t u32_StartAddress;

	// Gr��e festlegen
	u32_XYmodemFileLength = 2*FLASH_BLOCK_SIZE;

	// Startadresse berechnen
	u32_StartAddress = u8_BlockNum * FLASH_BLOCK_SIZE;

	v_SendStringTranslated(TEXT_BLOCK_START_TX_EN, TEXT_BLOCK_START_TX_DE);

	v_SetLedOn_2();
	u8_Status = u8_XmodemTxStartTransfer(u8_BlockData);

	if (u8_Status == XYMODEM_OK) {
		while (u32_FileLengthCounter > 0) {
			if (u32_FileLengthCounter >= 128)
				u8_DataSize = 128;
			else
				u8_DataSize = (uint8_t) u32_FileLengthCounter;

			// Datenblock bef�llen
			v_ReadMemoryDataArray(u8_BlockData, u8_DataSize/2, u32_StartAddress);
			u32_StartAddress += 64;

			u8_Error = u8_XYmodemSendFileData(u8_BlockData, 3, u8_DataSize, 128);
			if (u8_Error != XYMODEM_OK)
				// Fehler
				break;

			u32_FileLengthCounter -= u8_DataSize;
		}

		u8_Status = u8_XmodemTxCloseTransfer();
	}

	v_SetLedOff_2();

	if (u8_Error != XYMODEM_OK)
		return u8_Error;

	return u8_Status;
}

/**
 * Daten f�r einen Block �ber das X-Modem Protokoll empfangen und im Flash speichern
 * @param u8_BlockNum Blocknummer (0-3)
 * @param b_byteSwap true: High-/Low-Byte werden vertauscht
 * @return
 */
uint16_t u16_ReceiveBlock(uint8_t u8_BlockNum, bool b_byteSwap) {
	uint8_t u8_BlockData[128];
	uint16_t u16_Status;
	uint32_t u32_StartAddress;
	bool b_FirstPacket = true;

	v_UartRxBinaryTransfer(true);
	v_SetLedOn_2();

	// Startadresse berechnen
	u32_StartAddress = u8_BlockNum * FLASH_BLOCK_SIZE;

	v_XmodemRxInitTransfer(true);

	while(1) {
		u16_Status = u16_XYmodemGetFileData(u8_BlockData, b_FirstPacket, false);
		if (u16_Status == XYMODEM_RX_INFO_FILE_ONGOING) {
			// Dateigr��e wird in 8Bit Bytes �bertragen, wohingegen die Gr��e eines Flash Blocks in 16Bit W�rtern angegeben wird
			if (u32_XYmodemFileLength > (2*FLASH_BLOCK_SIZE)) {
				u16_Status = YMODEM_RX_ERR_WRONG_SIZE;
				break;
			}

			if (u16_XYmodemPacketSize > 0) {
				b_FirstPacket = false;

				if (!b_WriteMemoryDataArray(u8_BlockData, u16_XYmodemPacketSize / 2, u32_StartAddress, b_byteSwap)) {
					u16_Status = FLASH_WAIT_ERROR;
					break;
				}

				// Daten werden in 16Bit Schriten geschrieben, weshalb die neue Adresse immer nur der H�lfte der Paketgr��e (8Bit) entspricht
				u32_StartAddress += u16_XYmodemPacketSize / 2;
			}
			else
				// Fehler
				break;
		}
		else
			break;
	}

	v_SetLedOff_2();

	v_UartRxBinaryTransfer(false);
	return u16_Status;
}

/**
 * Daten ins EEProm �bernehmen
 */
void v_programEEprom(void) {
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
	while(NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);
}

/**
 * Byte in EEProm schreiben
 * @param u8_addr Adresse innerhalb des EEProm
 * @param u8_data Byte welches geschrieben werden soll
 */
void v_storeEEprom(uint8_t u8_addr, uint8_t u8_data) {
	uint8_t *pu8_ptr = (uint8_t *)MAPPED_EEPROM_START;
	*(pu8_ptr + u8_addr) = u8_data;
	v_programEEprom();
}

/**
 * Byte aus EEProm lesen
 * @param u8_addr Adresse innerhalb des EEProm
 * @return Wert im EEProm
 */
uint8_t u8_readEEprom(uint8_t u8_addr) {
	while(NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);

	uint8_t *eep_ptr = (uint8_t *)MAPPED_EEPROM_START;
	return *(eep_ptr + u8_addr);
}

/**
 * Vorselektierte Geschwindigkeit aus dem EEProm auslesen
 * @return 
 */
uint16_t u16_GetUsartBps() {
	uint8_t u8_Bps = u8_readEEprom(EEPROM_BPS_ADR);

	// Pr�fen ob MEM_RY_BY auf 0 liegt um default Baudrate wieder zu aktivieren
	if (b_IsMemBusy()) {
		v_storeEEprom(EEPROM_BPS_ADR, 0);
		u8_Bps = 0;
	}

	switch (u8_Bps) {
		case EEPROM_BPS_921600:
			return USART_BAUD_921600;
		case EEPROM_BPS_460800:
			return USART_BAUD_460800;
		case EEPROM_BPS_230400:
			return USART_BAUD_230400;
		case EEPROM_BPS_115200:
			return USART_BAUD_115200;
		case EEPROM_BPS_57600:
			return USART_BAUD_57600;
		case EEPROM_BPS_38400:
			return USART_BAUD_38400;
		case EEPROM_BPS_19200:
			return USART_BAUD_19200;
		case EEPROM_BPS_9600:
			return USART_BAUD_9600;
		default:
			return USART_BAUD_9600;
	}
}

/**
 * Reset durchf�hren
 */
static void v_reset(void) {
	v_UartSendString(TEXT_RESET);

	// Warten bis keine Zeichen mehr gesendet werden
	while(!b_UartIsTxEmpty());
	_delay_ms(10);

	// Reset durchf�hren
	_PROTECTED_WRITE(RSTCTRL.SWRR, RSTCTRL_SWRE_bm);
}

/**
 * Bootloader starten
 */
void v_enterBootloader(void) {
	v_SendStringTranslated(TEXT_BOOTLOADER_EN, TEXT_BOOTLOADER_DE);

	// Anforderung f�r den Bootloader speichern
	USERROW.USERROW31 = BOOTLOADER_REQUESTED;
	v_programEEprom();

	// Reset durchf�hren
	v_reset();
}

/**
 *  Zeige eine Fehlermeldung an, falls es beim Empfangen der Daten Fehler gab
 * u8_error = Fehlercode der seriellen Schnittstelle
 * return = true falls ein Fehler aufgetreten ist, ansonsten false
 */
bool b_showErrorCode(uint8_t u8_error) {
	if (u8_error != 0) {
		if (u8_error && SERIAL_ERROR_SW_BUFFER_OVERFLOW) {
			// Befehl ist zu lang
			v_UartClearRxBuffer();
			v_SendStringTranslated(TEXT_UART_VAL_TOO_LONG_EN, TEXT_UART_VAL_TOO_LONG_DE);
		}
		else if (u8_error && SERIAL_ERROR_HW_BUFFER_OVERFLOW)
			// Hardware Puffer ist �bergelaufen
			v_SendStringTranslated(TEXT_UART_HW_BUFFER_EN, TEXT_UART_HW_BUFFER_DE);
		else if (u8_error && SERIAL_ERROR_FRAME)
			// Frame wurde nicht erkannt
			v_UartSendString(TEXT_UART_FRAME);

		return true;
	}

	return false;
}

/**
 * Abfrage der Block Nummer mit Pr�fung ob die Block Nummer im g�ltigen Bereich liegt
 * @return Blocknummer (0-3)
 */
uint8_t u8_ReadBlockNum() {
	uint8_t u8_Counter;
	uint8_t u8_MaxBlock = 0x30;

	if (u8_ChipSize == CHIP_SIZE_1024KB)
		u8_MaxBlock = 0x31;
	else if (u8_ChipSize == CHIP_SIZE_2048KB)
		u8_MaxBlock = 0x33;

	v_SendStringTranslated(TEXT_BLOCK_SELECT1_EN, TEXT_BLOCK_SELECT1_DE);
	v_UartSendChar(u8_MaxBlock);
	v_UartSendString(TEXT_BLOCK_SELECT2);

	while (!b_UartIsNewlineDetected());
	u8_Counter = u8_UartGetRxVal();
	if (u8_Counter < 0x30 || u8_Counter > u8_MaxBlock) {
		// Ung�ltige Zahl eingegeben
		v_SendStringTranslated(TEXT_BLOCK_RANGE_EN, TEXT_BLOCK_RANGE_DE);
		v_UartSendChar(u8_MaxBlock);
		v_UartSendString(TEXT_NEWLINE);

		return 255;
	}

	v_UartSendString(TEXT_NEWLINE);
	return u8_Counter - 0x30;
}

/**
 * Pr�ft die internen Variablen ob der aktuelle Chip ein Flash ist und die Gr��e bereits bestimmt wurde
 * @param b_CheckType
 * @param b_CheckSize
 * @return true falls alles Pr�fungen ok sind
 */
bool b_CheckChipTypeSize(bool b_CheckType, bool b_CheckSize) {
	if (b_CheckType && u8_ChipType == CHIP_TYPE_27C400) {
		v_SendStringTranslated(TEXT_NOT_SUPPORTED_EN, TEXT_NOT_SUPPORTED_DE);
		return false;
	}

	if (b_CheckSize && u8_ChipSize == CHIP_SIZE_UNKNOWN) {
		v_SendStringTranslated(TEXT_IDENTIFY_EN, TEXT_IDENTIFY_DE);
		return false;
	}

	return true;
}

/**
 * Lese Flash Informationen
 * b_VerboseMode Ausf�hrliche Meldungen ausgeben
 */
void v_CmdReadFlashIdentification(bool b_VerboseMode) {
	uint8_t u8_Temp;
	uint16_t u16_Size, u16_Serial[4];
	char c_Value[6];

	u8_ChipSize = CHIP_SIZE_512KB;
	u8_ChipType = CHIP_TYPE_27C400;

	// CFI Abfrage starten
	if (!b_WriteMemoryCmdCfiQuery())
		v_SendStringTranslated(TEXT_CFI_START_ERR_EN, TEXT_CFI_START_ERR_DE);
	else {
		// QRY Zeichenfolge abfragen
		c_Value[0] = (char) u16_ReadMemoryAddress(CMD_CFI_DEVICE_QRY);
		c_Value[1] = (char) u16_ReadMemoryAddress(CMD_CFI_DEVICE_QRY+1);
		c_Value[2] = (char) u16_ReadMemoryAddress(CMD_CFI_DEVICE_QRY+2);
		c_Value[3] = 0;

		// Speichergr��e abfragen
		u16_Size = u16_ReadMemoryAddress(CMD_CFI_DEVICE_SIZE);

		// Seriennummer abfragen
		u16_Serial[0] = u16_ReadMemoryAddress(CMD_CFI_DEVICE_ID);
		u16_Serial[1] = u16_ReadMemoryAddress(CMD_CFI_DEVICE_ID + 1);
		u16_Serial[2] = u16_ReadMemoryAddress(CMD_CFI_DEVICE_ID + 2);
		u16_Serial[3] = u16_ReadMemoryAddress(CMD_CFI_DEVICE_ID + 3);

		// CFI Abfrage verlassen
		if (b_WriteMemoryCmdReadReset()) {
			// CFI Daten ausgeben
			if (b_VerboseMode)
				v_UartSendString(TEXT_CFI_QUERY_ID);

			if (!strcasecmp(TEXT_CFI_QUERY_ID_STRING, c_Value)) {
				if (b_VerboseMode) {
					v_UartSendString(c_Value);
					v_SendStringTranslated(TEXT_CFI_SIZE_EN, TEXT_CFI_SIZE_DE);
				}

				if (u16_Size == CFI_DEVICE_SIZE_512KB) {
					if (b_VerboseMode)
						v_UartSendString(TEXT_SIZE_512K);
					u8_ChipSize = CHIP_SIZE_512KB;
				}
				else if (u16_Size == CFI_DEVICE_SIZE_1024KB) {
					if (b_VerboseMode)
						v_UartSendString(TEXT_SIZE_1024K);
					u8_ChipSize = CHIP_SIZE_1024KB;
				}
				else if (u16_Size == CFI_DEVICE_SIZE_2048KB) {
					if (b_VerboseMode)
						v_UartSendString(TEXT_SIZE_2048K);
					u8_ChipSize = CHIP_SIZE_2048KB;
				}
				else {
					if (b_VerboseMode) {
						v_SendStringTranslated(TEXT_CFI_SIZE_UNKNOWN_EN, TEXT_CFI_SIZE_UNKNOWN_DE);
						v_Int2HexAscii(u16_Size, c_Value);
						v_UartSendString(c_Value);
					}
					u8_ChipSize = CHIP_SIZE_UNKNOWN;
				}

				if (b_VerboseMode) {
					v_UartSendString(TEXT_CFI_CHIP_ID);
					for (u8_Temp = 0; u8_Temp < 4; u8_Temp++) {
						v_Int2HexAscii(u16_Serial[u8_Temp], c_Value);
						v_UartSendString(c_Value);
					}
				}

				if (u8_ChipSize != CHIP_SIZE_UNKNOWN)
					u8_ChipType = CHIP_TYPE_FLASH_FT;
			}
			else if (b_VerboseMode)
				v_SendStringTranslated(TEXT_CFI_QUERY_ID_ERR_EN, TEXT_CFI_QUERY_ID_ERR_DE);
		}
		else
			v_SendStringTranslated(TEXT_CFI_END_ERR_EN, TEXT_CFI_END_ERR_DE);
	}

	v_SendStringTranslated(TEXT_CFI_TYPE_EN, TEXT_CFI_TYPE_DE);
	if (u8_ChipType == CHIP_TYPE_27C400)
		v_UartSendString(TEXT_CHIP_27C400);
	else
		v_UartSendString(TEXT_CHIP_FLASH_FT);

	v_UartSendString(TEXT_NEWLINE);
}

/**
 * Adresse abfragen und pr�fen ob diese innerhalb des g�ltigen Beeichs liegt
 * @return Eingegebene Adresse oder 0xFFFFFFFF, falls diese ung�ltig ist
 */
uint32_t u32_GetAddress() {
	uint8_t u8_Counter = 0;
	uint32_t u32_Address;
	char c_Value[6];

	v_SendStringTranslated(TEXT_GET_ADDR1_EN, TEXT_GET_ADDR1_DE);

	if (u8_ChipSize == CHIP_SIZE_512KB)
		v_UartSendString(TEXT_ADDR_RANGE_512K);
	else if (u8_ChipSize == CHIP_SIZE_1024KB)
		v_UartSendString(TEXT_ADDR_RANGE_1024K);
	else
		v_UartSendString(TEXT_ADDR_RANGE_2048K);

	v_UartSendString(TEXT_GET_ADDR2);

	while (!b_UartIsNewlineDetected());
	// Alle Empfangenen Zeichen abfragen

	while (!b_UartIsRxEmpty())
		c_Value[u8_Counter++] = u8_UartGetRxVal();
	// String Endezeichen erg�nzen
	c_Value[u8_Counter] = 0;

	// Adresse umwandeln
	u32_Address = u32_HexAscii2Long(c_Value);

	if (u8_ChipSize == CHIP_SIZE_512KB && u32_Address >= 0x40000) {
		v_SendStringTranslated(TEXT_ADDR_MAX1_EN, TEXT_ADDR_MAX1_DE);
		v_UartSendChar('3');
		v_SendStringTranslated(TEXT_ADDR_MAX2_EN, TEXT_ADDR_MAX2_DE);
		return 0xFFFFFFFF;
	}
	else if (u8_ChipSize == CHIP_SIZE_1024KB && u32_Address >= 0x80000) {
		v_SendStringTranslated(TEXT_ADDR_MAX1_EN, TEXT_ADDR_MAX1_DE);
		v_UartSendChar('7');
		v_SendStringTranslated(TEXT_ADDR_MAX2_EN, TEXT_ADDR_MAX2_DE);
		return 0xFFFFFFFF;
	}

	return u32_Address;
}

/**
 * Eine Adresse aus dem Chip auslesen
 */
void v_CmdReadAddress() {
	uint16_t u16_Data;
	uint32_t u32_Address;
	char c_Value[6];

	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(false, true)) {
		v_SetLedOff_2();
		return;
	}
	v_SetLedOff_2();

	u32_Address = u32_GetAddress();
	if (u32_Address == 0xFFFFFFFF)
		return;

	u16_Data = u16_ReadMemoryAddress(u32_Address);

	// High- und Low-Byte vertauschen damit es der Reihenfolge im 8Bit Modus entspricht
	u16_Data = u16_ChangeHighLowBytes(u16_Data);

	v_Int2HexAscii(u16_Data, c_Value);

	v_SendStringTranslated(TEXT_RD_ADDR_ENDIAN_EN, TEXT_RD_ADDR_ENDIAN_DE);
	v_UartSendString(c_Value);
	v_UartSendString(TEXT_NEWLINE);
}

/**
 * Eine Adresse in dem Chip beschreiben
 */
void v_CmdWriteAddress() {
	uint8_t u8_Counter = 0;
	uint16_t u16_Temp, u16_Data;
	uint32_t u32_Address;
	char c_Value[6];

	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(true, true)) {
		v_SetLedOff_2();
		return;
	}
	v_SetLedOff_2();

	u32_Address = u32_GetAddress();
	if (u32_Address == 0xFFFFFFFF)
		return;

	v_SendStringTranslated(TEXT_WR_ADDR_EMPTY_EN, TEXT_WR_ADDR_EMPTY_DE);
	u16_Temp = u16_ReadMemoryAddress(u32_Address);
	if (u16_Temp != 0xFFFF) {
		// High- und Low-Byte vertauschen damit es der Reihenfolge im 8Bit Modus entspricht
		u16_Temp = u16_ChangeHighLowBytes(u16_Temp);

		v_Int2HexAscii(u16_Temp, c_Value);

		v_SendStringTranslated(TEXT_WR_ADDR_EMPTY_ERR1_EN, TEXT_WR_ADDR_EMPTY_ERR1_DE);
		v_UartSendString(c_Value);
		v_SendStringTranslated(TEXT_WR_ADDR_EMPTY_ERR2_EN, TEXT_WR_ADDR_EMPTY_ERR2_DE);
		return;
	}
	v_UartSendString(TEXT_OK);

	v_SendStringTranslated(TEXT_WR_DATA_VALUE_EN, TEXT_WR_DATA_VALUE_DE);

	while (!b_UartIsNewlineDetected());

	// Alle Empfangenen Zeichen abfragen
	u8_Counter = 0;
	while (!b_UartIsRxEmpty())
		c_Value[u8_Counter++] = u8_UartGetRxVal();
	// String Endezeichen erg�nzen
	c_Value[u8_Counter] = 0;

	// Wert umwandeln
	u16_Data = u16_HexAscii2Int(c_Value);

	// High- und Low-Byte vertauschen damit es der Reihenfolge im 8Bit Modus entspricht
	u16_Data = u16_ChangeHighLowBytes(u16_Data);

	v_SendStringTranslated(TEXT_WR_DATA_ASK_EN, TEXT_WR_DATA_ASK_DE);

	while (!b_UartIsNewlineDetected());

	if (u8_UartGetRxVal() == 'y') {
		v_SetLedOn_2();
		if (b_WriteMemoryData(u32_Address, u16_Data)) {
			v_SendStringTranslated(TEXT_WR_VERIFY_EN, TEXT_WR_VERIFY_DE);
			u16_Temp = u16_ReadMemoryAddress(u32_Address);

			// Pr�fen ob der Wert im Flash identisch ist
			if (u16_Temp == u16_Data)
				v_UartSendString(TEXT_OK);
			else {
				// High- und Low-Byte vertauschen damit es der Reihenfolge im 8Bit Modus entspricht
				u16_Temp = u16_ChangeHighLowBytes(u16_Temp);

				// Im Flash steht ein anderer Wert als �bergeben wurde
				v_Int2HexAscii(u16_Temp, c_Value);

				v_SendStringTranslated(TEXT_WR_VERIFY_ERR_EN, TEXT_WR_VERIFY_ERR_DE);
				v_UartSendString(c_Value);
				v_UartSendString(TEXT_NEWLINE);
			}
		}
		else
			v_SendStringTranslated(TEXT_FAILED_EN, TEXT_FAILED_DE);

		v_SetLedOff_2();
	}
	else
		v_SendStringTranslated(TEXT_ABORT_EN, TEXT_ABORT_DE);
}

/**
 * Pr�fen ob ein Block leer ist
 * @param u8_blockNum Block Nummer
 * @return true falls der gesamte Block leer ist
 */
bool b_IsBlockEmpty(uint8_t u8_blockNum) {
	uint32_t u32_StartAddress, u32_Counter = 0;
	bool b_Empty = true;

	u32_StartAddress = u8_blockNum * FLASH_BLOCK_SIZE;

	v_SetLedOn_2();
	v_SetMemChipEnableOn();
	v_SetMemOutputEnableOn();
	v_SetShiftDataOutputOff();

	for (u32_Counter = 0; b_Empty && u32_Counter < FLASH_BLOCK_SIZE; u32_Counter++) {
		// Adresse ausgeben
		v_ShiftOutData(u32_StartAddress + u32_Counter, 0);
		// Daten einlesen
		if (u16_ShiftInData() != 0xFFFF)
			b_Empty = false;

		if ((u32_Counter & 0xFFFF) == 0xFFFF)
			v_UartSendChar('.');
	}

	v_SetMemChipEnableOff();
	v_SetLedOff_2();

	return b_Empty;
}

/**
 * Pr�fen ob ein Block leer ist mit Abfrage des Blocks
 */
void v_CmdCheckBlock() {
	uint8_t u8_BlockNum;

	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(false, true)) {
		v_SetLedOff_2();
		return;
	}
	v_SetLedOff_2();

	u8_BlockNum = u8_ReadBlockNum();
	if (u8_BlockNum == 255)
		return;

	v_SendStringTranslated(TEXT_BLOCK_CHECK_EN, TEXT_BLOCK_CHECK_DE);

	if (b_IsBlockEmpty(u8_BlockNum))
		v_SendStringTranslated(TEXT_TRUE_EN, TEXT_TRUE_DE);
	else
		v_SendStringTranslated(TEXT_FALSE_EN, TEXT_FALSE_DE);
}

/**
 * Pr�fen ob der gesamte Chip leer ist
 */
bool b_CmdCheckChip() {
	uint8_t u8_Counter, u8_NumBlocks;

	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(false, true)) {
		v_SetLedOff_2();
		return false;
	}
	v_SetLedOff_2();

	v_SendStringTranslated(TEXT_CHIP_EMPTY_EN, TEXT_CHIP_EMPTY_DE);

	if (u8_ChipSize == CHIP_SIZE_512KB)
		u8_NumBlocks = 1;
	else if (u8_ChipSize == CHIP_SIZE_1024KB)
		u8_NumBlocks = 2;
	else
		u8_NumBlocks = 4;

	for (u8_Counter = 0; u8_Counter < u8_NumBlocks; u8_Counter++) {
		if (!b_IsBlockEmpty(u8_Counter)) {
			v_SendStringTranslated(TEXT_FALSE_EN, TEXT_FALSE_DE);
			return false;
		}
	}

	v_UartSendString(TEXT_OK);
	return true;
}

/**
 * Einen 256k x 16Bit Block aus dem Chip auslesen
 */
void v_CmdReadBlock() {
	uint8_t u8_BlockNum;
	uint16_t u16_Temp;
	char c_Value[6];

	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(false, true)) {
		v_SetLedOff_2();
		return;
	}
	v_SetLedOff_2();

	v_SendStringTranslated(TEXT_BLOCK_RD_MODE_EN, TEXT_BLOCK_RD_MODE_DE);
	u8_BlockNum = u8_ReadBlockNum();
	if (u8_BlockNum == 255)
		return;

	v_SendStringTranslated(TEXT_BLOCK_RD_START_EN, TEXT_BLOCK_RD_START_DE);

	u16_Temp = u8_TransmitBlock(u8_BlockNum);

	v_DelayCounter();

	if (u16_Temp == XYMODEM_OK) {
		v_SendStringTranslated(TEXT_BLOCK_RD_OK_EN, TEXT_BLOCK_RD_OK_DE);
		v_setFlashReadCount();
	}
	else {
		v_SendStringTranslated(TEXT_BLOCK_RD_ERR_EN, TEXT_BLOCK_RD_ERR_DE);
		v_Int2HexAscii(u16_Temp, c_Value);
		v_UartSendString(c_Value);
	}

	v_UartSendString(TEXT_NEWLINE);
}

/**
 * Einen 256k x 16 Block im Flash l�schen
 * return: true, falls der Block gel�scht werden soll
 */
bool b_CmdEraseBlock(uint8_t u8_BlockNum) {
	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(true, true)) {
		v_SetLedOff_2();
		return false;
	}
	v_SetLedOff_2();

	// Pr�fen ob ein Block mitgegeben wurde oder abgefragt werden soll
	if (u8_BlockNum == 0xFF) {
		// Block abfragen
		v_SendStringTranslated(TEXT_BLOCK_ERASE_MODE_EN, TEXT_BLOCK_ERASE_MODE_DE);
		u8_BlockNum = u8_ReadBlockNum();
		if (u8_BlockNum == 255)
			return false;
	}

	v_SendStringTranslated(TEXT_BLOCK_ERASE_ASK_EN, TEXT_BLOCK_ERASE_ASK_DE);

	while (!b_UartIsNewlineDetected());

	if (u8_UartGetRxVal() == 'y') {
		if (b_WriteMemoryBlockErase(u8_BlockNum)) {
			v_SendStringTranslated(TEXT_BLOCK_ERASE_OK_EN, TEXT_BLOCK_ERASE_OK_DE);
			return true;
		}
		else
			v_SendStringTranslated(TEXT_BLOCK_ERASE_ERR_EN, TEXT_BLOCK_ERASE_ERR_DE);
	}
	else {
		v_UartSendString(TEXT_NEWLINE);
		v_SendStringTranslated(TEXT_ABORT_EN, TEXT_ABORT_DE);
	}

	return false;
}

/**
 * Den kompletten Flash l�schen
 */
void v_CmdEraseChip() {
	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(true, true)) {
		v_SetLedOff_2();
		return;
	}
	v_SetLedOff_2();

	v_SendStringTranslated(TEXT_CHIP_ERASE_ASK_EN, TEXT_CHIP_ERASE_ASK_DE);

	while (!b_UartIsNewlineDetected());

	if (u8_UartGetRxVal() == 'y') {
		if (b_WriteMemoryChipErase())
			v_SendStringTranslated(TEXT_CHIP_ERASE_OK_EN, TEXT_CHIP_ERASE_OK_DE);
		else
			v_SendStringTranslated(TEXT_CHIP_ERASE_ERR_EN, TEXT_CHIP_ERASE_ERR_DE);
	}
	else {
		v_UartSendString(TEXT_NEWLINE);
		v_SendStringTranslated(TEXT_ABORT_EN, TEXT_ABORT_DE);
	}
}

/**
 * Einen 256k x 16Bit Block in den Flash schreiben
 */
void v_CmdWriteBlock() {
	uint8_t u8_BlockNum, u8_Temp;
	uint16_t u16_Temp;
	char c_Value[6];
	bool b_byteSwap;

	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(true, true)) {
		v_SetLedOff_2();
		return;
	}
	v_SetLedOff_2();

	v_SendStringTranslated(TEXT_BLOCK_WR_MODE_EN, TEXT_BLOCK_WR_MODE_DE);
	u8_BlockNum = u8_ReadBlockNum();
	if (u8_BlockNum == 255)
		return;

	v_SendStringTranslated(TEXT_BLOCK_CHECK_EN, TEXT_BLOCK_CHECK_DE);
	if (b_IsBlockEmpty(u8_BlockNum))
		v_SendStringTranslated(TEXT_TRUE_EN, TEXT_TRUE_DE);
	else {
		v_SendStringTranslated(TEXT_FALSE_EN, TEXT_FALSE_DE);

		// Abfragen ob der Block gel�scht werden soll
		if (!b_CmdEraseBlock(u8_BlockNum))
			return;
	}

	v_SendStringTranslated(TEXT_BLOCK_WR_SWAP_ASK_EN, TEXT_BLOCK_WR_SWAP_ASK_DE);

	while (!b_UartIsNewlineDetected());

	u8_Temp = u8_UartGetRxVal();
	if (u8_Temp == 'r')
		b_byteSwap = true;
	else if (u8_Temp == 'b')
		b_byteSwap = false;
	else {
		// Unbekannte Taste gedr�ckt
		v_SendStringTranslated(TEXT_ABORT_EN, TEXT_ABORT_DE);
		return;
	}

	v_SendStringTranslated(TEXT_BLOCK_WR_START_EN, TEXT_BLOCK_WR_START_DE);
	u16_Temp = u16_ReceiveBlock(u8_BlockNum, b_byteSwap);

	v_DelayCounter();

	if (u16_Temp == XYMODEM_RX_INFO_FILE_FINISHED) {
		v_SendStringTranslated(TEXT_BLOCK_WR_OK_EN, TEXT_BLOCK_WR_OK_DE);
		v_setFlashWriteCount();
	}
	else {
		v_SendStringTranslated(TEXT_BLOCK_WR_ERR_EN, TEXT_BLOCK_WR_ERR_DE);
		v_Int2HexAscii(u16_Temp, c_Value);
		v_UartSendString(c_Value);
		v_UartSendString(TEXT_NEWLINE);
	}
}

/**
 * Gesamten Chip mit den angegebenen Daten beschreiben
 * @param c_TestNum Nummerierung des Tests, welche in der Ausgabe angezeigt wird
 * @param u16_WriteData Zu schreibende 16Bit Daten
 * @return 
 */
bool b_TestChipWrite(char c_TestNum, uint16_t u16_WriteData) {
	uint16_t u16_ReadData;
	uint32_t u32_Address, u32_Size;
	char c_Str[9];

	// Pr�fen ob der Chip leer ist
	v_UartSendString(TEXT_NEWLINE);
	v_UartSendChar(c_TestNum);
	v_UartSendString(". ");
	if (!b_CmdCheckChip())
		return false;

	// Flash Gr��e ermitteln
	u32_Size = 0;
	if (u8_ChipSize == CHIP_SIZE_512KB)
		u32_Size = 0x40000;
	else if (u8_ChipSize == CHIP_SIZE_1024KB)
		u32_Size = 0x80000;
	else if (u8_ChipSize == CHIP_SIZE_2048KB)
		u32_Size = 0x100000;

	v_Int2HexAscii(u16_WriteData, c_Str);

	// Gesamten Chip mit Daten beschreiben
	v_UartSendChar(c_TestNum);
	v_SendStringTranslated(TEXT_CHIP_TEST_WR1_EN, TEXT_CHIP_TEST_WR1_DE);
	v_UartSendString(c_Str);
	v_SendStringTranslated(TEXT_CHIP_TEST_WR2_EN, TEXT_CHIP_TEST_WR2_DE);
	v_SetLedOn_2();
	for (u32_Address = 0; u32_Address < u32_Size; u32_Address++) {
		if (!b_WriteMemoryData(u32_Address, u16_WriteData)) {
			v_SendStringTranslated(TEXT_FAILED_EN, TEXT_FAILED_DE);
			return false;
		}

		if ((u32_Address & 0xFFFF) == 0xFFFF)
			v_UartSendChar('.');
	}
	v_SetLedOff_2();
	v_UartSendString(TEXT_OK);

	// Geschriebene Daten pr�fen
	v_UartSendChar(c_TestNum);
	v_SendStringTranslated(TEXT_CHIP_TEST_VERIFY_EN, TEXT_CHIP_TEST_VERIFY_DE);
	v_SetLedOn_2();
	for (u32_Address = 0; u32_Address < u32_Size; u32_Address++) {
		u16_ReadData = u16_ReadMemoryAddress(u32_Address);
		if (u16_ReadData != u16_WriteData) {
			v_Long2HexAscii(u32_Address, c_Str);
			v_SendStringTranslated(TEXT_CHIP_TEST_VERIFY_ERR1_EN, TEXT_CHIP_TEST_VERIFY_ERR1_DE);
			v_UartSendString(c_Str);

			v_Int2HexAscii(u16_ReadData, c_Str);
			v_SendStringTranslated(TEXT_CHIP_TEST_VERIFY_ERR2_EN, TEXT_CHIP_TEST_VERIFY_ERR2_DE);
			v_UartSendString(c_Str);
			v_UartSendString(TEXT_NEWLINE);

			return false;
		}

		if ((u32_Address & 0xFFFF) == 0xFFFF)
			v_UartSendChar('.');
	}
	v_SetLedOff_2();
	v_UartSendString(TEXT_OK);

	// Gesamten Chip l�schen
	v_UartSendChar(c_TestNum);
	v_SendStringTranslated(TEXT_CHIP_TEST_ERASE_EN, TEXT_CHIP_TEST_ERASE_DE);
	if (!b_WriteMemoryChipErase()) {
		v_SendStringTranslated(TEXT_FAILED_EN, TEXT_FAILED_DE);
		return false;
	}
	v_UartSendString(TEXT_OK);

	return true;
}

/**
 * Chip schreiben/lesen/l�schen testen
 */
void v_CmdTestChip() {
	bool b_Test;

	v_SendStringTranslated(TEXT_CHIP_TEST_ASK_EN, TEXT_CHIP_TEST_ASK_DE);
	while (!b_UartIsNewlineDetected());

	if (u8_UartGetRxVal() != 'y') {
		v_SendStringTranslated(TEXT_ABORT_EN, TEXT_ABORT_DE);
		return;
	}

	// Chip identifizieren
	v_SetLedOn_2();
	v_CmdReadFlashIdentification(false);

	if (!b_CheckChipTypeSize(true, true)) {
		v_SetLedOff_2();
		return;
	}

	v_SendStringTranslated(TEXT_CHIP_TEST_ERASE2_EN, TEXT_CHIP_TEST_ERASE2_DE);
	if (!b_WriteMemoryChipErase()) {
		v_SetLedOff_2();
		v_SendStringTranslated(TEXT_FAILED_EN, TEXT_FAILED_DE);
		return;
	}
	v_UartSendString(TEXT_OK);

	// Verschiede Muster schreiben und Pr�fen
	b_Test = b_TestChipWrite('1', 0x0000);

	if (b_Test)
		b_Test = b_TestChipWrite('2', 0x12DE);

	v_SetLedOff_2();

	v_SendStringTranslated(TEXT_CHIP_TEST_RESULT_EN, TEXT_CHIP_TEST_RESULT_DE);
	if (b_Test)
		v_UartSendString(TEXT_OK);
	else
		v_SendStringTranslated(TEXT_FAILED_EN, TEXT_FAILED_DE);
}

/**
 * Aktuellen Chip Typ und Gr��e anzeigen
 */
void v_ShowChipType() {
	v_SendStringTranslated(TEXT_SHOW_CHIP_EN, TEXT_SHOW_CHIP_DE);
	if (u8_ChipType == CHIP_TYPE_27C400)
		v_UartSendString(TEXT_CHIP_27C400);
	else if (u8_ChipType == CHIP_TYPE_FLASH_BT)
		v_UartSendString(TEXT_CHIP_FLASH_BT);
	else if (u8_ChipType == CHIP_TYPE_FLASH_FT)
		v_UartSendString(TEXT_CHIP_FLASH_FT);

	v_SendStringTranslated(TEXT_SHOW_CHIP_SIZE_EN, TEXT_SHOW_CHIP_SIZE_DE);
	if (u8_ChipSize == CHIP_SIZE_UNKNOWN)
		v_SendStringTranslated(TEXT_SIZE_UNKNOWN_EN, TEXT_SIZE_UNKNOWN_DE);
	else if (u8_ChipSize == CHIP_SIZE_512KB)
		v_UartSendString(TEXT_SIZE_512K);
	else if (u8_ChipSize == CHIP_SIZE_1024KB)
		v_UartSendString(TEXT_SIZE_1024K);
	else
		v_UartSendString(TEXT_SIZE_2048K);
	v_UartSendString(TEXT_NEWLINE);
}

/**
 * Geschwindigkeit der seriellen Schnittstelle festlegen
 */
void v_CmdSetUsartBps() {
	uint8_t u8_input;

	v_SendStringTranslated(TEXT_SERIAL_BPS_ASK_EN, TEXT_SERIAL_BPS_ASK_DE);
	v_UartSendString(TEXT_SERIAL_BPS_OPTIONS);
	while (!b_UartIsNewlineDetected());

	u8_input = u8_UartGetRxVal();
	u8_input -= 0x30;
	if (u8_input >= EEPROM_BPS_921600 && u8_input <= EEPROM_BPS_9600) {
		// Neue Geschwindigkeit speichern
		v_storeEEprom(EEPROM_BPS_ADR, u8_input);
		v_SendStringTranslated(TEXT_SERIAL_BPS_REBOOT_EN, TEXT_SERIAL_BPS_REBOOT_DE);

		// Uart neu initialisieren
		_delay_ms(10);
		v_UartInit(&SERIAL_TINY160X_PORT, SERIAL_TINY160X_TXD, SERIAL_TINY160X_RXD, u16_GetUsartBps());
	}
	else
		v_SendStringTranslated(TEXT_ABORT_EN, TEXT_ABORT_DE);
}

/**
 * Vorselektierte Geschwindigkeit aus dem EEProm auslesen und anzeigen
 */
void v_ShowUsartBps() {
	uint16_t u16_Bps = u16_GetUsartBps();

	v_SendStringTranslated(TEXT_SERIAL_BPS_SHOW_EN, TEXT_SERIAL_BPS_SHOW_DE);
	switch (u16_Bps) {
		case (uint16_t) USART_BAUD_921600:
			v_UartSendString(TEXT_SERIAL_BPS_921600);
			break;
		case (uint16_t) USART_BAUD_460800:
			v_UartSendString(TEXT_SERIAL_BPS_460800);
			break;
		case (uint16_t) USART_BAUD_230400:
			v_UartSendString(TEXT_SERIAL_BPS_230400);
			break;
		case (uint16_t) USART_BAUD_115200:
			v_UartSendString(TEXT_SERIAL_BPS_115200);
			break;
		case (uint16_t) USART_BAUD_57600:
			v_UartSendString(TEXT_SERIAL_BPS_57600);
			break;
		case (uint16_t) USART_BAUD_38400:
			v_UartSendString(TEXT_SERIAL_BPS_38400);
			break;
		case (uint16_t) USART_BAUD_19200:
			v_UartSendString(TEXT_SERIAL_BPS_19200);
			break;
		case (uint16_t) USART_BAUD_9600:
			v_UartSendString(TEXT_SERIAL_BPS_9600);
			break;
		default:
			v_UartSendString(TEXT_SERIAL_BPS_9600);
	}

	v_UartSendString(TEXT_NEWLINE);
}

/**
 * Sprache abfragen und im EEProm speichern
 */
void v_CmdSetLanguage() {
	uint8_t u8_input;

	v_SendStringTranslated(TEXT_LANGUAGE_ASK_EN, TEXT_LANGUAGE_ASK_DE);
	while (!b_UartIsNewlineDetected());

	u8_input = u8_UartGetRxVal();
	u8_input -= 0x30;
	if (u8_input >= EEPROM_LANG_EN && u8_input <= EEPROM_LANG_DE) {
		// Neue Sprache speichern
		v_storeEEprom(EEPROM_LANG_ADR, u8_input);

		if (u8_input == EEPROM_LANG_EN)
			b_LangEn = true;
		else
			b_LangEn = false;

		v_SendStringTranslated(TEXT_LANGUAGE_OK_EN, TEXT_LANGUAGE_OK_DE);
		return;
	}

	v_SendStringTranslated(TEXT_ABORT_EN, TEXT_ABORT_DE);
}

/**
 * Sprache aus dem EEprom laden
 */
void v_getLanguage() {
	uint8_t u8_Lang = u8_readEEprom(EEPROM_LANG_ADR);

	switch (u8_Lang) {
		case EEPROM_LANG_EN:
			b_LangEn = true;
			break;
		case EEPROM_LANG_DE:
			b_LangEn = false;
			break;
		default:
			b_LangEn = true;
	}
}

/**
 * Anzahl erfolgreich gelesener/geschriebener Chips aus dem EEprom laden
 */
void v_getFlashReadWriteCount() {
	// High Byte laden
	u16_CountReadCycles = u8_readEEprom(EEPROM_FLASH_RD_COUNT_ADR + 1);
	// High Byte verschieben
	u16_CountReadCycles <<= 8;
	// Low Byte laden und addieren
	u16_CountReadCycles += u8_readEEprom(EEPROM_FLASH_RD_COUNT_ADR);

	if (u16_CountReadCycles == 0xFFFF)
		// Noch kein Wert eingetragen
		u16_CountReadCycles = 0;

	// High Byte laden
	u16_CountWriteCycles = u8_readEEprom(EEPROM_FLASH_WR_COUNT_ADR + 1);
	// High Byte verschieben
	u16_CountWriteCycles <<= 8;
	// Low Byte laden und addieren
	u16_CountWriteCycles += u8_readEEprom(EEPROM_FLASH_WR_COUNT_ADR);

	if (u16_CountWriteCycles == 0xFFFF)
		// Noch kein Wert eingetragen
		u16_CountWriteCycles = 0;
}

/**
 * Anzahl erfolgreich durchgef�hrter Leseoperationen um eins erh�hen und intern speichern
 */
void v_setFlashReadCount() {
	uint8_t u8_Temp;

	u16_CountReadCycles++;

	// Low Byte laden
	u8_Temp = (uint8_t) (u16_CountReadCycles & 0xFF);
	// Low Byte speichern
	v_storeEEprom(EEPROM_FLASH_RD_COUNT_ADR, u8_Temp);
	// High Byte laden
	u8_Temp = (uint8_t) ((u16_CountReadCycles >> 8) & 0xFF);
	// High Byte speichern
	v_storeEEprom(EEPROM_FLASH_RD_COUNT_ADR+1, u8_Temp);
}

/**
 * Anzahl erfolgreich durchgef�hrter Schreibperationen um eins erh�hen und intern speichern
 */
void v_setFlashWriteCount() {
	uint8_t u8_Temp;

	u16_CountWriteCycles++;

	u8_Temp = (uint8_t) (u16_CountWriteCycles & 0xFF);
	v_storeEEprom(EEPROM_FLASH_WR_COUNT_ADR, u8_Temp);
	u8_Temp = (uint8_t) ((u16_CountWriteCycles >> 8) & 0xFF);
	v_storeEEprom(EEPROM_FLASH_WR_COUNT_ADR+1, u8_Temp);
}

/**
 * Anzahl erfolgreich durchgef�hrter Lese-/Schreibperationen anzeigen
 */
void v_CmdShowFlashCount() {
	char c_Str[5];

	v_SendStringTranslated(TEXT_FLASH_COUNT_RD_EN, TEXT_FLASH_COUNT_RD_DE);
	v_Int2HexAscii(u16_CountReadCycles, c_Str);
	v_UartSendString(c_Str);
	v_UartSendString(TEXT_NEWLINE);

	v_SendStringTranslated(TEXT_FLASH_COUNT_WR_EN, TEXT_FLASH_COUNT_WR_DE);
	v_Int2HexAscii(u16_CountWriteCycles, c_Str);
	v_UartSendString(c_Str);
	v_UartSendString(TEXT_NEWLINE);
}

void v_showInformation() {
	// Version anzeigen
	v_UartSendString(TEXT_CMD_VERSION);
	v_UartSendString(VERSION);
	v_UartSendString(TEXT_NEWLINE);
	// Aktuellen Chip Typ und Gr��e anzeigen
	v_ShowChipType();
	// Aktuell gew�hlte Geschwindigkeit der seriellen Schnittstelle anzeigen
	v_ShowUsartBps();
	// Anzahl erfolgreich durchgef�hrter Lese-/Schreiboperatioen anzeigen
	v_CmdShowFlashCount();
	v_UartSendString(TEXT_NEWLINE);
}

/**
 * Serielle Befehle verarbeiten
 */
void v_commandParser(void) {
	uint8_t u8_Temp;
	uint8_t u8_counter = 0;
	char ac_command[MAX_LENGTH_RXBUF + 1];

	u8_Temp = u8_UartGetErrorCode();
	if (!b_showErrorCode(u8_Temp)) {
		// Alle Empfangenen Zeichen abfragen
		while (!b_UartIsRxEmpty())
			ac_command[u8_counter++] = u8_UartGetRxVal();
		// String Endezeichen erg�nzen
		ac_command[u8_counter] = 0;

		// String zu Lowercase
		strlwr(ac_command);

		if (!strcasecmp(CMD_HELP, ac_command)) {
			v_showInformation();

			v_UartSendString(CMD_HELP);
			v_SendStringTranslated(TEXT_HELP_CMD_HELP_EN, TEXT_HELP_CMD_HELP_DE);
			v_UartSendString(CMD_CHIP_INFO);
			v_SendStringTranslated(TEXT_HELP_CMD_CHIP_INFO_EN, TEXT_HELP_CMD_CHIP_INFO_DE);
			v_UartSendString(CMD_READ_ADDR);
			v_SendStringTranslated(TEXT_HELP_CMD_READ_ADDR_EN, TEXT_HELP_CMD_READ_ADDR_DE);
			v_UartSendString(CMD_WRITE_ADDR);
			v_SendStringTranslated(TEXT_HELP_CMD_WRITE_ADDR_EN, TEXT_HELP_CMD_WRITE_ADDR_DE);
			v_UartSendString(CMD_READ_BLOCK);
			v_SendStringTranslated(TEXT_HELP_CMD_READ_BLOCK_EN, TEXT_HELP_CMD_READ_BLOCK_DE);
			v_UartSendString(CMD_WRITE_BLOCK);
			v_SendStringTranslated(TEXT_HELP_CMD_WRITE_BLOCK_EN, TEXT_HELP_CMD_WRITE_BLOCK_DE);
			v_UartSendString(CMD_ERASE_BLOCK);
			v_SendStringTranslated(TEXT_HELP_CMD_ERASE_BLOCK_EN, TEXT_HELP_CMD_ERASE_BLOCK_DE);
			v_UartSendString(CMD_ERASE_CHIP);
			v_SendStringTranslated(TEXT_HELP_CMD_ERASE_CHIP_EN, TEXT_HELP_CMD_ERASE_CHIP_DE);
			v_UartSendString(CMD_CHECK_BLOCK);
			v_SendStringTranslated(TEXT_HELP_CMD_CHECK_BLOCK_EN, TEXT_HELP_CMD_CHECK_BLOCK_DE);
			v_UartSendString(CMD_CHECK_CHIP);
			v_SendStringTranslated(TEXT_HELP_CMD_CHECK_CHIP_EN, TEXT_HELP_CMD_CHECK_CHIP_DE);
			v_UartSendString(CMD_TEST_CHIP);
			v_SendStringTranslated(TEXT_HELP_CMD_TEST_CHIP_EN, TEXT_HELP_CMD_TEST_CHIP_DE);
			v_UartSendString(CMD_SERIAL_BPS);
			v_SendStringTranslated(TEXT_HELP_CMD_SERIAL_BPS_EN, TEXT_HELP_CMD_SERIAL_BPS_DE);
			v_UartSendString(CMD_LANGUAGE);
			v_SendStringTranslated(TEXT_HELP_CMD_LANG_EN, TEXT_HELP_CMD_LANG_DE);
		}
		else if (!strcasecmp(CMD_BOOTLOADER, ac_command))
			// Bootloader starten
			v_enterBootloader();
		else if (!strcasecmp(CMD_RESET, ac_command))
			// Reset durchf�hren
			v_reset();
		else if (!strcasecmp(CMD_VERSION, ac_command)) {
			// Version anzeigen
			v_UartSendString(TEXT_CMD_VERSION);
			v_UartSendString(VERSION);
			v_UartSendString(TEXT_NEWLINE);
		}
		else if (!strcasecmp(CMD_CHIP_INFO, ac_command))
			// Chip Informationen auslesen
			v_CmdReadFlashIdentification(true);
		else if (!strcasecmp(CMD_READ_ADDR, ac_command))
			// Adresse auslesen
			v_CmdReadAddress();
		else if (!strcasecmp(CMD_WRITE_ADDR, ac_command))
			// Adresse beschreiben
			v_CmdWriteAddress();
		else if (!strcasecmp(CMD_READ_BLOCK, ac_command))
			// Block auslesen
			v_CmdReadBlock();
		else if (!strcasecmp(CMD_WRITE_BLOCK, ac_command))
			// Block schreiben
			v_CmdWriteBlock();
		else if (!strcasecmp(CMD_ERASE_BLOCK, ac_command))
			// Block l�schen
			b_CmdEraseBlock(0xFF);
		else if (!strcasecmp(CMD_ERASE_CHIP, ac_command))
			// Chip l�schen
			v_CmdEraseChip();
		else if (!strcasecmp(CMD_CHECK_BLOCK, ac_command))
			// Pr�fen ob ein Block leer ist
			v_CmdCheckBlock();
		else if (!strcasecmp(CMD_CHECK_CHIP, ac_command))
			// Pr�fen ob der gesamte Chip leer ist
			b_CmdCheckChip();
		else if (!strcasecmp(CMD_TEST_CHIP, ac_command))
			// Chip testen
			v_CmdTestChip();
		else if (!strcasecmp(CMD_SERIAL_BPS, ac_command))
			// Serielle Geschwindigkeit festlegen
			v_CmdSetUsartBps();
		else if (!strcasecmp(CMD_LANGUAGE, ac_command))
			// Serielle Geschwindigkeit festlegen
			v_CmdSetLanguage();
		else {
			// Unbekannter Befehl
			v_SendStringTranslated(TEXT_CMD_ERR_EN, TEXT_CMD_ERR_DE);
			v_UartSendString(ac_command);
			v_UartSendString(TEXT_NEWLINE);

			v_SendStringTranslated(TEXT_CMD_HELP1_EN, TEXT_CMD_HELP1_DE);
			v_UartSendString(CMD_HELP);
			v_UartSendString(TEXT_CMD_HELP2);
		}
	}
}

int main(void) {
	v_init();

	v_SetLedOff_1();
	v_SetLedOff_2();

	v_DelayCounter();

	// Betriebsanzeige
	v_SetLedOn_1();

	v_SendStringTranslated(TEXT_PROGRAM_START_EN, TEXT_PROGRAM_START_DE);
	v_UartSendString(TEXT_COPYRIGHT);
	v_UartSendString(TEXT_NEWLINE);

	v_showInformation();

	v_SendStringTranslated(TEXT_CMD_HELP1_EN, TEXT_CMD_HELP1_DE);
	v_UartSendString(CMD_HELP);
	v_UartSendString(TEXT_CMD_HELP2);

	v_SetMemOutputEnableOff();
	v_SetShiftAddressOutputOn();

	v_UartClearRxBuffer();
	while(1) {
		if (b_UartIsNewlineDetected())
			v_commandParser();
	}
}

/******************************************************** Interruptfunktionen ********************************************************/

// Serieller RX Interrupt
ISR(SERIAL_RX_ISR) {
	while (SERIAL_STATUS & USART_RXCIF_bm)
		v_UartSetRxVal(SERIAL_RX_DATAH, SERIAL_RX_DATA);
}

// Serieller TX Interrupt
ISR(SERIAL_TX_ISR) {
	// Interrupt Flag zur�cksetzen
	SERIAL_STATUS |= USART_TXCIF_bm;

	if (!b_UartIsTxEmpty())
		// Sendeprozess starten
		SERIAL_TX_DATA = u8_UartGetTxVal();
	else
		b_UartTxInProgress = false;
}
